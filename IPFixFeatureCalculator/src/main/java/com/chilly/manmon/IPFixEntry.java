package com.chilly.manmon;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;
import java.util.ArrayList;

public class IPFixEntry
{
  private Instant time;
  private String  dst_ip;
  private String  src_ip;
  private int     dst_port;
  private int     src_port;
  private int     bytes;
  
  public IPFixEntry(IPFixEntryMapped orig)
  {
    time = orig.getTime();
    dst_ip = orig.getDst_ip();
    src_ip = orig.getSrc_ip();
    dst_port = Integer.parseInt(orig.getDst_port());
    src_port = Integer.parseInt(orig.getSrc_port());
    bytes = Integer.parseInt(orig.getBytes());
  }
  
  public String toString()
  {
    return "["
               .concat(time.toString())
               .concat("]From:")
               .concat(src_ip)
               .concat("|To:")
               .concat(dst_ip)
               .concat("|Bytes:" + bytes);
  }
}
