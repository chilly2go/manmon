package com.chilly.manmon;

import com.chilly.manmon.tasks.IPFixDateRetriever;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import java.util.List;
import java.util.concurrent.*;

public class Main extends Thread
{
    public static final int                             QUEUE_BUFFER                 = 2500;
    public static final int                             IPFIX_RETRIEVE_CHUNKSIZE     = 10;
    public static final int                             THREADPOOL_SIZE              = 4;
    public static final int                             THREADPOOL_SHUTDOWN_WAITTIME = 5;
    private final       CountDownLatch                  latch;
    private final       LinkedBlockingQueue<IPFixEntry> queue;
    private             ExecutorService                 executorService;
    
    public Main()
    {
        this.latch = new CountDownLatch(1);
        this.executorService = Executors.newFixedThreadPool(Main.THREADPOOL_SIZE);
        this.queue = new LinkedBlockingQueue<IPFixEntry>(Main.QUEUE_BUFFER);
    }
    
    // threads:
//    - get new data from influx if queue runs low
//    - - input new data into structure
//    - - add new data to be analyzed
//    - delete old data
//    - write new feature vector to influx
    public static void main(String[] args)
    {
        CountDownLatch wait = new CountDownLatch(1);
        Main           main = new Main();
        Runtime.getRuntime().addShutdownHook(main);
        main.startJobs();
        try
        {
            main.getLatch().await();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
    
    private void startJobs()
    {
        this.executorService.submit(new IPFixDateRetriever(this.queue, this.latch, this.executorService));
    }
    
    private CountDownLatch getLatch()
    {
        return this.latch;
    }
    
    @Override
    public void run()
    {
        this.executorService.shutdown();
        this.latch.countDown();
        try
        {
            this.executorService.awaitTermination(Main.THREADPOOL_SHUTDOWN_WAITTIME, TimeUnit.SECONDS);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
