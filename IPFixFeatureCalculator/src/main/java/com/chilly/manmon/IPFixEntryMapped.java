package com.chilly.manmon;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;
import java.util.ArrayList;

@Measurement(name = "ipfix")
public class IPFixEntryMapped
{
  @Column(name = "time")
  private Instant time;
  @Column(name = "to_ip", tag = true)
  private String  dst_ip;
  @Column(name = "to_port", tag = true)
  private String  dst_port;
  @Column(name = "from_ip", tag = true)
  private String  src_ip;
  @Column(name = "from_port", tag = true)
  private String  src_port;
  @Column(name = "bytes")
  private String  bytes;
  
  
  public String getBytes()
  {
    return bytes;
  }
  
  public String getSrc_port()
  {
    return src_port;
  }
  
  public String getDst_port()
  {
    return dst_port;
  }
  
  public String getDst_ip()
  {
    return dst_ip;
  }
  
  public Instant getTime()
  {
    return time;
  }
  
  public String getSrc_ip()
  {
    return src_ip;
  }
}
