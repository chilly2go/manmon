package com.chilly.manmon.tasks;

import com.chilly.manmon.IPFixEntry;
import com.chilly.manmon.IPFixEntryMapped;
import com.chilly.manmon.Main;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

public class IPFixDateRetriever extends Thread
{
  private final InfluxDB                        influxDB;
  private       LinkedBlockingQueue<IPFixEntry> toAnalyze;
  private       LinkedBlockingQueue<IPFixEntry> toWrite;
  private       LinkedBlockingDeque<IPFixEntry> dataStore;
  private       CountDownLatch                  latch;
  private       String                          dbName = "snmp_metrics";
  private       ExecutorService                 exec;
  
  public IPFixDateRetriever(LinkedBlockingQueue<IPFixEntry> toAnalyze, CountDownLatch latch,
                            ExecutorService exec
                           )
  {
    this.exec = exec;
    this.influxDB = InfluxDBFactory.connect("http://influxpi.heiderich.home:8086");
    this.toAnalyze = toAnalyze;
    this.latch = latch;
  }
  
  @Override
  public void run()
  {
    while (this.latch.getCount() > 0)
    {
      QueryResult queryResult =
          this.influxDB.query(new Query(
              "SELECT time,bytes,from_ip,from_port,to_ip,to_port FROM ipfix order by time ASC Limit " +
              Main.IPFIX_RETRIEVE_CHUNKSIZE,
              dbName));
      InfluxDBResultMapper   resultMapper         = new InfluxDBResultMapper(); // thread-safe - can be reused
      List<IPFixEntryMapped> mappedIPFixEntryList = resultMapper.toPOJO(queryResult, IPFixEntryMapped.class);
      for (int i = 0; i < mappedIPFixEntryList.size(); i++)
      {
        this.dataStore.add(new IPFixEntry(mappedIPFixEntryList.get(i)));
        var tmp = new IPFixEntryFeatureCalcJob(this.dataStore.getLast(), this.dataStore, this.toWrite);
        tmp.run();
//        this.exec.submit(tmp);
      }
    }
    this.influxDB.close();
  }
}
