package com.chilly.manmon.tasks;

import com.chilly.manmon.IPFixEntry;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

public class IPFixEntryFeatureCalcJob implements Runnable
{
  private IPFixEntry                      entry;
  private LinkedBlockingDeque<IPFixEntry> dataStore;
  
  public IPFixEntryFeatureCalcJob(IPFixEntry entry,
                                  LinkedBlockingDeque<IPFixEntry> dataStore,
                                  LinkedBlockingQueue<IPFixEntry> toWrite
                                 )
  {
    this.entry = entry;
    this.dataStore = dataStore;
  }
  
  @Override
  public void run()
  {
  
  }
}
