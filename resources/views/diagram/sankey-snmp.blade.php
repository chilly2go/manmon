@extends('layouts.app')

@section('content')
	<div class="card card-default">
		<div class="card-header d-inline-flex justify-content-between">
			<span class="form-inline">SNMP-Traffic Übersicht der
			letzten {!! Form::text('minutes',30,['class' => "form-control col-1 mx-1 px-2","id" => "timeparam"]) !!}
			                          Minuten <em id="last-update" class="mx-2 text-secondary"></em >
			</span >
			<span id="updateButton">
				<i class="fas fa-redo-alt"></i >
			</span >
		</div >
		<div class="card-body">
			<div
					id="myDiagramDiv" style="xbackground-color: #696969; border: solid 1px black; width: 99%; height: 850px"
					param="snmp"
			></div >
		</div >
	</div >
	@push('scripts')
		<script src="{{ asset('js/sankey.js') }}"></script >
	@endpush
@endsection
