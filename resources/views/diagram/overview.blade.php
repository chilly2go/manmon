@extends('layouts.app')

@section('content')
	<div class="card card-default">
		<div class="card-header d-inline-flex justify-content-between">
			<span >Status: <em id="last-update" class="mx-2 text-secondary"></em ></span >
			<span id="updateButton">
				<i class="fas fa-redo-alt"></i >
			</span >
		</div >
		<div class="card-body">
			<div id="myDiagramDiv" class="col-12 mx-3" style="height: 95vh"></div >
		</div >
	</div >
	@push('scripts')
		<script src="{{ asset('js/forceDirectedDiagram.js') }}"></script >
	@endpush
@endsection
