@extends('layouts.app')

@section('content')
	<div class="card card-default">
		<div class="card-header">
			<span >Interface des SNMP Endgeräts auswählen:</span >
		</div >
		<div class="card-body">
			{!! Form::open(['route' => 'connection.create.step3']) !!}
			{!! Form::hidden('from',$values['from']) !!}
			{!! Form::hidden('to',$values['to']) !!}
			
			<div class="row">
				{{-- from --}}
				<div class="col">
					@if($from->endpoint != NULL)
						{!! Form::select('from_interface', [$from->endpoint->interfaces->pluck('nameInfo','id')->toArray()], NULL, ['class' => 'form-control form-control-lg']) !!}
					@else
						{!! Form::text(NULL,$from->nameInfo,['class' => 'form-control','disabled'=>'disabled']) !!}
					@endif
				</div >
				{{-- to --}}
				<div class="col">
					@if($to->endpoint != NULL)
						{!! Form::select('to_interface', [$to->endpoint->interfaces->pluck('nameInfo','id')->toArray()], NULL, ['class' => 'form-control form-control-lg']) !!}
					@else
						{!! Form::text(NULL,$to->nameInfo,['class' => 'form-control','disabled']) !!}
					@endif
				</div >
			</div >
			<div class="row">
				<div class="col my-1">
					{!! Form::button('<i class="far fa-hdd"></i > Speichern',['class' => 'btn btn-primary mb-2','type' => 'submit']) !!}
				</div >
			</div >
			{!! Form::close() !!}
		</div >
	</div >
@endsection
