@extends('layouts.app')

@section('content')
	<div class="card card-default">
		@include('connections.create-step2-header')
		<div class="card-body">
			An welchem Anschluss besteht die Verbindung?
			{!! Form::open(['route' => 'connection.create.endpoint']) !!}
			<div class="row">
				<div class="col">
					{!! Form::hidden('endpoint',$values['endpoint']) !!}
					{!! Form::hidden('to_ip',$values['to_ip']) !!}
					{!! Form::select('interface', [$endpoint->interfaces->pluck('name','id')->toArray()], NULL, ['class' => 'form-control form-control-lg']) !!}
				</div >
			</div >
			<div class="row">
				<div class="col my-1">
					{!! Form::button('<i class="far fa-hdd"></i > Speichern',['class' => 'btn btn-primary mb-2','type' => 'submit']) !!}
				</div >
			</div >
			{!! Form::close() !!}
		</div >
	</div >
@endsection
