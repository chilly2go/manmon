@extends('layouts.app')

@section('content')
	<div class="card card-default">
		<div class="card-header d-inline-flex justify-content-between">
			<span >Verbindungen:</span >
			<a href="{{route('connection.create.step1')}}" class="mx-3"> <i class="fas fa-link"></i > Neue Verbindung anlegen</a >
		</div >
		<div class="card-body">
			<div class="list-group">
				@forelse($connections as $connection)
					<div class="list-group-item list-group-item-action">
						<div class="row">
							<div class="col-11">
								<div class="row">
									<span class="col">
										{{ optional($connection->fromEndpointInterface)->endpoint->nameInfo ?? $connection->fromIP->nameInfo  }}
									</span >
									<span class="col-1 text-success"><i class="fas fa-link"></i ></span >
									<span class="col">
										{{ optional($connection->toEndpointInterface)->endpoint->name ?? $connection->toIP->nameInfo  }}
									</span >
								</div >
							</div >
							<div class="col-1 d-inline-flex justify-content-end">
								<a href="{{route("connection.delete",['connection'=>$connection->id])}}" class="text-warning">
									<i class="fas fa-unlink"></i >
								</a >
							</div >
						</div >
					</div >
				@empty
					nix da
				@endforelse
				{{ $connections->links() }}
			</div >
		</div >
	</div >
@endsection
