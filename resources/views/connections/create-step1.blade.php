@extends('layouts.app')

@section('content')
	<div class="card card-default">
		<div class="card-header">
			<span >Neue Verbindung anlegen:</span >
		</div >
		<div class="card-body">
			{!! Form::open(['route' => 'connection.create.step2']) !!}
			<div class="row">
				{{-- from --}}
				<div class="col">
					{!! Form::select('from', [$ips->pluck('nameInfo','id')], NULL, ['class' => 'form-control form-control-lg']) !!}
				</div >
				{{-- to --}}
				<div class="col">
					{!! Form::select('to', [$ips->pluck('nameInfo','id')->toArray()], NULL, ['class' => 'form-control form-control-lg']) !!}
				</div >
			</div >
			<div class="row">
				<div class="col my-1">
					{!! Form::button('<i class="far fa-upload"></i > Speichern',['class' => 'btn btn-primary mb-2','type' => 'submit']) !!}
				</div >
			</div >
		</div >
	</div >
@endsection
