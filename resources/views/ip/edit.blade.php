@extends('layouts.app')

@section('content')
	<div class="card card-default w-100">
		<div class="card-header d-flex justify-content-between">
			<h3 >Registrierte IP Adresse: {{$ip->ip}}</h3 >
			<span >
				<a href="{{ route("ip.index") }}">
					<i class="fas fa-level-up"></i > Zurück
				</a >
			</span >
		</div >
		<div class="card-body">
			{!! Form::open(['method' => 'POST', 'route' => ['ip.update',$ip->id]]) !!}
			<div class="row">
				<div class="form-group col">
					<label for="edit_ip">IP-Adresse</label >
					<input
							type="text" class="form-control" id="edit_ip" name="edit_ip" aria-describedby="ipHelp" value="{{$ip->ip}}"
					>
					<small id="ipHelp" class="form-text text-muted">Muss eindeutig sein.</small >
				</div >
				<div class="form-group col">
					<label for="edit_name">Name</label >
					<input
							type="text" class="form-control" id="edit_name" name="edit_name" aria-describedby="nameHelp"
							value="{{$ip->name}}" placeholder="{{optional($ip->endpoint)->name}}"
					>
					<small id="nameHelp" class="form-text text-muted">Name bzw. Bezeichnung für diese IP-Adresse. Ist
					                                                  eine IP mit einem Endgerät verknüpft, wird der Endgerätname
					                                                  hier vorgeschlagen.
					</small >
				</div >
				<div class="form-group col form-check form-check-inline">
					<label class="form-check-label">Regelmäßiger Ping?</label >
					{!! Form::select('should_ping', [0 => 'Nein', 1 => 'Ja', 2 => 'Ja, ist extern'], $ip->should_ping,['class' => 'form-control']) !!}
					{{--{!! Form::checkbox('should_ping', 'yes', $ip->should_ping,['class' => 'form-check-input']) !!}--}}
				
				</div >
			</div >
			<div class="d-flex justify-content-end">
				{!! Form::submit("Speichern", ['class' => 'btn btn-success mt-2']) !!}
			</div >
			{!! Form::close() !!}
		</div >
	</div >
@endsection
