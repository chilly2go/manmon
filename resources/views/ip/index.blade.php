@extends('layouts.app')

@section('content')
	<div class="card card-default w-100">
		<div class="card-header d-flex justify-content-between">
			<h3 >Registrierte IP Adressen</h3 >
			<span >
                {!! Form::open(['method' => 'POST', 'route' => 'ip.store', 'class' => 'form-inline']) !!}
				<input type="text" name="new_ip" class="form-control mb-2 mr-sm-2" placeholder="127.0.0.1">
                    <button type="submit" class="btn btn-primary mb-2"><i class="far fa-plus-square"></i > </button >
				{!! Form::close() !!}
            </span >
		</div >
		<div class="card-body">
			<table class="table table-hover">
				<thead >
				<tr >
					<th scope="col">#</th >
					<th scope="col">IP</th >
					<th scope="col">Name / Bezeichnung</th >
					<th scope="col">Aktion</th >
				</tr >
				</thead >
				<tbody >
				@forelse($ips as $ip)
					<tr >
						<th scope="row">{{$loop->iteration}}</th >
						<td >{{$ip->ip}}</td >
						<td >
							<div class="d-flex justify-content-between">
								<span >{{$ip->name }}</span >
								<div class="extras d-flex justify-content-end col-5">
                  <span
		                  class="ml-4 d-flex justify-content-between col-4" data-toggle="popover"
		                  data-content="{{optional(optional($ip->endpoint)->interfaces())->count() ?: 0 }} Schnittstellen"
                  >
                    <i class="fal fa-desktop-alt"></i >
                    ({{optional(optional($ip->endpoint)->interfaces())->count() ?: 0 }} )
                  </span >
									<div class="col-4">
										@if($ip->should_ping>0)
											<div
													class="ping-result d-flex justify-content-between" data-toggle="popover"
													data-content="Ping-Latenz: {{number_format($ip->latency,2,',','.')}} ms"
											>
												<span ><i class="fas fa-compress-alt"></i ></span >
												<span >{{number_format($ip->latency,2,',','.')}} ms</span >
											</div >
										@endif
									</div >
								</div >
							</div >
						</td >
						<td class="cols">
							<a href="{{ route("ip.edit",['ip' => $ip->id]) }}" data-toggle="popover" data-content="IP bearbeiten">
								<i class="far fa-edit fa-fw fa-lg"></i >
							</a > &nbsp; <a
									href="{{ route("ip.destroy",['ip' => $ip->id]) }}" data-toggle="popover" data-content="IP löschen"
							>
								<i class="far fa-eraser fa-fw fa-lg"></i >
							</a >
						</td >
					</tr >
				@empty
				@endforelse
				</tbody >
			</table >
			{{ $ips->links() }}
		</div >
	</div >
@endsection
