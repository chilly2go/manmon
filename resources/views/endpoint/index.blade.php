@extends('layouts.app')

@section('content')
	<div class="card card-default w-100">
		<div class="card-header d-flex justify-content-between">
			<h3 >Bekannte Netzwerkgeräte</h3 >
			<span >
        {!! Form::open(['method' => 'POST', 'route' => 'endpoint.store', 'class' => 'form-inline']) !!}
				{!! Form::text('ip',NULL,["placeholder" => "127.0.0.1", 'class' => 'form-control mb-2 mr-sm-2' ]) !!}
				{!! Form::text('community',NULL,["placeholder" => "public", 'class' => 'form-control mb-2 mr-sm-2' ]) !!}
				{!! Form::button('<i class="far fa-plus-square"></i > ',
								['class'      => 'btn btn-primary mb-2',
								'data-toggle' => 'popover',
								'data-content'=> 'Neues Endgerät anlegen',
								'type'        => 'submit']) !!}
				{!! Form::close() !!}
            </span >
		</div >
		<div class="card-body">
			<table class="table table-hover">
				<thead >
				<tr >
					<th scope="col">#</th >
					<th scope="col">Name</th >
					<th scope="col">Ort</th >
					<th scope="col">Uptime</th >
					<th scope="col">IP</th >
					<th scope="col">Aktion</th >
				</tr >
				</thead >
				<tbody >
				@forelse($endpoints as $endpoint)
					<tr >
						<th scope="row">{{$loop->iteration}}</th >
						<td >
							<div class="d-flex justify-content-between">
								<span >{{$endpoint->name }}</span >
								<span
										class="ml-4 d-flex justify-content-between col-3" data-toggle="popover"
										data-content="{{optional($endpoint->interfaces())->count() ?: 0 }} Schnittstellen"
								>
                    <i class="fal fa-desktop-alt"></i >
                    ({{optional($endpoint->interfaces())->count() ?: 0 }} )
                  </span >
							</div >
						</td >
						<td >{{ $endpoint->ort }}</td >
						<td >{!! \App\Helpers\Timespan::timespanHtml($endpoint->uptime??0) !!}</td >
						<td >{{ $endpoint->ip->ip }}</td >
						<td class="cols">
							<a
									href="{{ route("endpoint.edit",['endpoint' => $endpoint->id]) }}" data-toggle="popover"
									data-content="SNMP Gerät bearbeiten"
							>
								<i class="far fa-edit fa-fw fa-lg"></i >
							</a > &nbsp; <a
									href="{{ route("endpoint.destroy",['endpoint' => $endpoint->id]) }}" data-toggle="popover"
									data-content="SNMP Gerät löschen"
							>
								<i class="far fa-eraser fa-fw fa-lg"></i >
							</a >
						</td >
					</tr >
				@empty
				@endforelse
				</tbody >
			</table >
			{{ $endpoints->links() }}
		</div >
	</div >
@endsection
