@extends('layouts.app')

@section('content')
	<div class="card card-default w-100">
		<div class="card-header d-flex justify-content-between">
			<h3 >Netzgerät: {{$endpoint->name}}</h3 >
			<span >
				<a href="{{ route("endpoint.index") }}">
					<i class="fas fa-level-up"></i > Zurück
				</a >
			</span >
		</div >
		<div class="card-body">
			{!! Form::open(['method' => 'POST', 'route' => ['endpoint.update',$endpoint->id]]) !!}
			<div class="row">
				<div class="form-group col">
					{!! Form::label('name','Name') !!}
					{!! Form::text('name',$endpoint->name,['class' => 'form-control']) !!}
				</div >
				<div class="form-group col">
					{!! Form::label('community','SNMP Community') !!}
					{!! Form::text('community',$endpoint->community,['class' => 'form-control']) !!}
				</div >
			</div >
			<div class="row border-top py-1 mx-0 mt-1">
				<small >Update alle 15 Minuten</small >
			</div >
			<div class="row">
				<div class="form-group col">
					Beschreibung: {{ $endpoint->beschreibung }}
				</div >
				<div class="form-group col">
					Uptime: {!! \App\Helpers\Timespan::timespanHtml($endpoint->uptime) !!}
				</div >
				<div class="form-group col">
					Kontakt: {{ $endpoint->kontakt }}
				</div >
				<div class="form-group col">
					Ort: {{ $endpoint->ort }}
				</div >
			</div >
			<div class="d-flex justify-content-end">
				{!! Form::submit("Speichern", ['class' => 'btn btn-success mt-2']) !!}
			</div >
		</div >
		{!! Form::close() !!}
		@if($endpoint->interfaces->count() >0)
			<div class="card-body border-top">
				<small >Neue Werte sind minütlich verfügbar
					@if(config("monitoring.snmp.poll.datastore", "mysql") == "influx")
					      (Stand:1 {{$endpoint->getTimeForInterface($endpoint->interfaces->first()->id)}})
					@else
				        (Stand: {{$endpoint->updated_at}})
					@endif
				</small >
				<table class="table table-hover">
					<thead >
					<tr >
						<th scope="col">Interface Index</th >
						<th scope="col">Name</th >
						<th scope="col">In</th >
						<th scope="col">Out</th >
						<th scope="col">Total</th >
					</tr >
					</thead >
					<tbody >
					@foreach($endpoint->interfaces as $interface)
						<tr >
							<td scope="row">{{$interface->index}}</td >
							<td scope="row">{{$interface->name}}</td >
							@if(config("monitoring.snmp.poll.datastore", "mysql") == "influx")
								<td scope="row">{{\App\Helpers\DataRate::calc($endpoint->getRateInForInterface($interface->id))}}</td >
								<td scope="row">{{\App\Helpers\DataRate::calc($endpoint->getRateOutForInterface($interface->id))}}</td >
								<td scope="row">{{\App\Helpers\DataRate::calc($endpoint->getRateCombinedForInterface($interface->id))}}</td >
							@else
								<td scope="row">{{\App\Helpers\DataRate::calc($interface->rateIn)}}</td >
								<td scope="row">{{\App\Helpers\DataRate::calc($interface->rateOut)}}</td >
								<td scope="row">{{\App\Helpers\DataRate::calc($interface->rateCombined)}}</td >
							@endif
						</tr >
					@endforeach
					</tbody >
				</table >
			</div >
		@endif
	</div >
@endsection
