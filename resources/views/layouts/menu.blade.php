<!-- Sidebar  -->
<nav id="sidebar">
	<div class="sticky-top sidebar-height d-flex flex-column">
		<div class="sidebar-header sidebarCollapse p-3">
			<h3 >Monitoring</h3 >
			<strong ><i class="fas fa-binoculars"></i > </strong >
		</div >
		<div class="d-flex flex-column justify-content-between mb-auto">
			<ul class="list-unstyled components">
				<li class="active has-sub">
					<a href="#homeSubmenu">
						<i class="far fa-broadcast-tower fa-fw"></i >
						<span class="link-text">Netzwerk</span >
					</a >
					<ul class="list-unstyled rounded-right" id="homeSubmenu">
						<li >
							<a href="{{route("ip.index")}}">IP</a >
						</li >
						<li >
							<a href="{{route("endpoint.index")}}">Endgeräte</a >
						</li >
						<li >
							<a href="{{route("connection.index")}}">Verbindungen</a >
						</li >
					</ul >
				</li >
				<li class="has-sub">
					<a href="#">
						<i class="far fa-chart-line fa-fw"></i >
						<span class="link-text">Diagramm</span >
					</a >
					<ul class="list-unstyled">
						<li >
							<a href="{{route("diagram.overview")}}">Übersicht</a >
						</li >
						<li >
							<a href="{{route("diagram.sankey.ipfix")}}">Traffic-Verteilung</a >
						</li >
						<li >
							<a href="{{route("diagram.sankey.snmp")}}">Traffic-Verteilung (SNMP)</a >
						</li >
					</ul >
				</li >
				<!--<li>
						<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
								<i class="fas fa-copy"></i>
								Pages
						</a>
						<ul class="collapse list-unstyled" id="pageSubmenu">
								<li>
										<a href="#">Page 1</a>
								</li>
								<li>
										<a href="#">Page 2</a>
								</li>
								<li>
										<a href="#">Page 3</a>
								</li>
						</ul>
				</li>-->
			</ul >
		</div >
		<div class="border-top collapser-hover">
            <span class="sidebarCollapse collapser py-2 pl-1 mr-1">
                <span class="do-collapse w-100"><i class="far fa-angle-double-left fa-fw"></i > Einklappen</span >
                <span class="expand w-100"><i class="far fa-angle-double-right fa-fw"></i > </span >
            </span >
		</div >
	</div >
</nav >
