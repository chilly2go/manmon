<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head >
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<title >Monitoring: Heiderich</title >
	
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	
	<script defer src="{{asset('js/fontawesome.js')}}"></script >
</head >

<body >
<div class="wrapper app" id="app">
@include("layouts.menu")
<!-- Page Content  -->
	<div id="content" class="my-2 col">
		<vue-snotify ></vue-snotify >
	@if(session("errors"))
		@foreach (session("errors")->all() as $error)
			<!--<div class="alert alert-warning" role="alert">{{ $error }}</div>-->
				<snotify ntype="warning" ntitle="Warnung" ntext="'{!! $error !!}'"></snotify >
			@endforeach
		@endif
		@if(session("status"))
			@if(is_array(session("status")))
				@foreach (session("status") as $info)
					<snotify ntype="info" ntitle="Hinweis" ntext="'{!! $info !!}'"></snotify >
				@endforeach
			@else
				<snotify ntype="info" ntitle="Hinweis" ntext="'{!! session("status") !!}'"></snotify >
			@endif
		@endif
		
		
		@yield('content')
	</div >
</div >

<script src="{{ asset('js/app.js') }}"></script >
<script src="{{ asset('js/custom.js') }}"></script >
@stack('scripts')
</body >

</html >