
/*
 enable popovers
 */
$(function ()
  {
    $("[data-toggle=\"popover\"]").popover({trigger: "hover"});
  });

/*
 allow sidebar to (un-)collapse
 */
$(document).ready(
  function ()
  {

    $(".sidebarCollapse").on("click", function ()
    {
      $("#sidebar").toggleClass("active");
    });

  });