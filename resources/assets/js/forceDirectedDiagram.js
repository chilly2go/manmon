import go from "gojs";

var myDiagram;

function init ()
{
  if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
  var $ = go.GraphObject.make;  // for conciseness in defining templates

  myDiagram =
    $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
      {
        initialAutoScale: go.Diagram.UniformToFill,  // an initial automatic zoom-to-fit
        contentAlignment: go.Spot.TopCenter,  // align document to the center of the viewport
        layout          :
        // $(go.TreeLayout,  // automatically spread nodes apart
        //   {nodeSpacing: 45, angle: 90, layerSpacing: 80, layerStyle: go.TreeLayout.LayerUniform}),
        //
          $(go.LayeredDigraphLayout,  // automatically spread nodes apart
            {
              direction        : 90,
              columnSpacing    : 20,
              cycleRemoveOption: go.LayeredDigraphLayout.CycleGreedy,
              initializeOption : go.LayeredDigraphLayout.InitDepthFirstOut,
              iterations       : 7,
              layeringOption   : go.LayeredDigraphLayout.LayerOptimalLinkLength,
              layerSpacing     : 50,
              packOption:go.LayeredDigraphLayout.PackNone
            }),
        //
        //   $(go.ForceDirectedLayout,  // automatically spread nodes apart
        //     {maxIterations: 250, defaultSpringLength: 45, defaultElectricalCharge: 100, defaultSpringStiffness:
        // 0.1}),
      });

  // define each Node's appearance
  myDiagram.nodeTemplate =
    $(go.Node, "Auto",  // the whole node panel
      {locationSpot: go.Spot.Center},
      // define the node's outer shape, which will surround the TextBlock
      $(go.Shape, "Rectangle",
        {fill: $(go.Brush, "Linear", {0: "rgb(254, 201, 0)", 1: "rgb(254, 162, 0)"}), stroke: "black"}),
      $(go.TextBlock,
        {font: "bold 11pt helvetica, bold arial, sans-serif", margin: 4},
        new go.Binding("text", "text")),
    );

  // replace the default Link template in the linkTemplateMap
  myDiagram.linkTemplate =
    $(go.Link,  // the whole link panel
      {routing: go.Link.AvoidsNodes, corner: 7, layerName: "Foreground", reshapable: true, curve: go.Link.JumpOver},
      $(go.Shape,  // the link shape
        {stroke: "black"}),
      $(go.Shape,  // the arrowhead
        {fromArrow: "Backward", toArrow: "", stroke: null}),
      // $(go.Panel, "Horizontal", {},
      //   $(go.Shape,  // the label background, which becomes transparent around the edges
      //     {
      //       fill  : $(go.Brush, "Radial",
      //                 {0: "rgb(240, 240, 240)", 0.2: "rgb(240, 240, 240)", 1: "rgba(240, 240, 240, 0)"}),
      //       stroke: null,
      //     }),
      // ),
      $(go.TextBlock,  // the label text
        {
          textAlign    : "center",
          font         : "10pt helvetica, arial, sans-serif",
          stroke       : "#333333",
          margin       : 4,
          segmentIndex : -3,
          segmentOffset: new go.Point(NaN, NaN),
          // segmentOrientation: go.Link.OrientUpright,
        },
        new go.Binding("text", "text")),
    );
  load();
}

function load ()
{
  // create the model for the concept map
  var nodeDataArray = [
    {key: 1, text: "Concept Maps"},
    {key: 2, text: "Organized Knowledge"},
    {key: 3, text: "Context Dependent"},
    {key: 4, text: "Concepts"},
    {key: 5, text: "Propositions"},
    {key: 6, text: "Associated Feelings or Affect"},
    {key: 7, text: "Perceived Regularities"},
    {key: 8, text: "Labeled"},
    {key: 9, text: "Hierarchically Structured"},
    {key: 10, text: "Effective Teaching"},
    {key: 11, text: "Crosslinks"},
    {key: 12, text: "Effective Learning"},
    {key: 13, text: "Events (Happenings)"},
    {key: 14, text: "Objects (Things)"},
    {key: 15, text: "Symbols"},
    {key: 16, text: "Words"},
    {key: 17, text: "Creativity"},
    {key: 18, text: "Interrelationships"},
    {key: 19, text: "Infants"},
    {key: 20, text: "Different Map Segments"},
  ];
  var linkDataArray = [
    {from: 1, to: 2, text: "represent"},
    {from: 2, to: 3, text: "is"},
    {from: 2, to: 4, text: "is"},
    {from: 2, to: 5, text: "is"},
    {from: 2, to: 6, text: "includes"},
    {from: 2, to: 10, text: "necessary\nfor"},
    {from: 2, to: 12, text: "necessary\nfor"},
    {from: 4, to: 5, text: "combine\nto form"},
    {from: 4, to: 6, text: "include"},
    {from: 4, to: 7, text: "are"},
    {from: 4, to: 8, text: "are"},
    {from: 4, to: 9, text: "are"},
    {from: 5, to: 9, text: "are"},
    {from: 5, to: 11, text: "may be"},
    {from: 7, to: 13, text: "in"},
    {from: 7, to: 14, text: "in"},
    {from: 7, to: 19, text: "begin\nwith"},
    {from: 8, to: 15, text: "with"},
    {from: 8, to: 16, text: "with"},
    {from: 9, to: 17, text: "aids"},
    {from: 11, to: 18, text: "show"},
    {from: 12, to: 19, text: "begins\nwith"},
    {from: 17, to: 18, text: "needed\nto see"},
    {from: 18, to: 20, text: "between"},
  ];
  axios.get("/diagram/overview/data").then(
    rsp =>
    {
      console.log("ajax update :)");
      // myDiagram.model = go.Model.fromJson(rsp.data.diagram);
      myDiagram.model = new go.GraphLinksModel(rsp.data.diagram.nodeDataArray, rsp.data.diagram.linkDataArray);
      $("em#last-update").html(rsp.data.updated);
    },
  );
  // myDiagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
  console.log("GoGoGo");
}

document.getElementById("updateButton").onclick = function ()
{
  console.log("Update triggered");
  load();
};

init();