<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 02.08.2018
 * Time: 16:17
 */

return [

    "paginate" => ["items" => env("PAGINATION_ITEMS_PER_PAGE", 7)],

];