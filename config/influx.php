<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 01.08.2018
 * Time: 22:15
 */
return [

    "host"   => env("INFLUX_DB_HOST", "localhost"),
    "port"   => env("INFLUX_DB_PORT", 8086),
    "dbname" => env("INFLUX_DB_NAME", "snmp_metrics"),

];