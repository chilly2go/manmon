<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 31.08.2018
 * Time: 12:12
 */

return ["diagram"   =>
    
            ["overview" =>
        
                 ["filter"      => TRUE,
                  "dummy-entry" =>
            
                      ["linkdata" =>
                
                           [
                    
                               ["from" => 0, "to" => 16, "text" => ""],
                               ["from" => 0, "to" => 17, "text" => ""],
                
                           ],
                       "nodedata" => ["key" => 0, "text" => "Übersicht"],
            
                      ],
                  "max"         => FALSE,
        
                 ],
    
             "snmp" => [
        
                 /*
                  * Zeige Verbindungen von Geräte Ports mit Traffic ohne Zuordnung zu empfangendem Gerät?
                  */
                 "show_unknown" => TRUE]],
        'scheduler' => [
    
            'enabled' => TRUE,

        ],
        'snmp'      => [
    
            'poll' => ['enabled'   => TRUE,
                       // datastores: influx / mysql
                       'datastore' => "mysql",
                       "mysql"     => ['log_affected_rows' => TRUE]],

        ],
        'ping'      => [
    
            'enabled'  => TRUE,
            'dispatch' => ['delay' => 2],

        ],
        'horizon'   => [
    
            'statistics' => ['enabled' => TRUE,],

        ],
        /**********************************************************
         * CSV started as conversion from binary IPFix data to csv.
         * Processing steps were added to:
         * - Chunk the data to write it to influx
         * - Due to impractical ram requirement by influx a switch to mysql as datastore was made
         * - As we ran into Deadlocks while serialising the call to calc features was halted
         **********************************************************/
        'csv'       => [
    
            'check-files' => ['enabled' => TRUE,
                              'delay'   => 15,
                              'cols'    => ['from' => 0,
                                            'to'   => 15]],
            'line-chunks' => ['type'   => "mysql",
                              'influx' => ['delay' => 5, 'size' => 50,],
                              'mysql'  => ['delay'             => 1,
                                           'size'              => 50,
                                           'log_affected_rows' => TRUE,
                                           'calc_features'     => FALSE],
    
            ],

        ],
        'lookup'    => [/*
             * Available Endpoints:
             * - IP API (Seems to be out of service since 2018-09-23
             * - IP DATA
             **/
                        'endpoint'  => "IPDATA",
                        'endpoints' => ["IPAPI"  => [],
                                        "IPDATA" => ["API_KEY" => "63144321b1cd19d22627e85232e16a8f26ae94442dd3191d70e1a08f"]],
                        'check'     => ['enabled' => TRUE, 'chunk-size' => 100],

        ],

        'service-mappings' => ['import-file' => "netflow/PortServiceMapping.txt",
                               'delimiter'   => '|',
                               'malware'     => ['keywords' => ['trojan',
                                                                'rootkit',
                                                                'worm',
                                                                'virus',
                                                                'Trojan',
                                                                'Rootkit',
                                                                'Worm',
                                                                'Virus']]],

];