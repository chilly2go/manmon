<?php

namespace App;

use App\Helpers\InfluxDataInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SNMP_Endpoint extends Model
{
    protected $fillable = ['ip_id',
                           "community",
                           "beschreibung",
                           "uptime",
                           "kontakt",
                           "name",
                           "ort",];
    private   $interfaceRatesObject;
    
    public
    function connectionsIPFrom()
    {
        return $this->hasMany("App\EndpointConnection", "ip_from_id", "ip_id");
    }
    
    public
    function connectionsIPTo()
    {
        return $this->hasMany("App\EndpointConnection", "ip_to_id", "ip_id");
    }
    
    public
    function interfaces()
    {
        return $this->hasMany("App\EndpointData", "endpoint_id", "id");
    }
    
    public
    function ip()
    {
        return $this->belongsTo("App\IP_Pool", "ip_id");
    }
    
    private
    function checkInterfaceRates()
    {
        if ($this->interfaceRatesObject == NULL)
        {
            try
            {
                $this->interfaceRatesObject =
                    
                    InfluxDataInterface::getDatabase()
                                       ->getQueryBuilder()
                                       ->select('interface_id,rate_in,rate_out,rate_combined')
                                       ->from('interface_metric')
                                       ->where(["endpoint_id = '" . $this->id . "'"])
                                       ->orderBy('time', 'DESC')
                                       ->limit($this->interfaces->count())
                                       ->getResultSet()
                                       ->getPoints();
            } catch (InfluxDB\Client\Exception $e)
            {
                dd($e);
                Log::channel("influx")->error($e->getMessage());
            } catch (\Exception $e)
            {
                dd($e, "exception");
            }
        }
    }
    
    private
    function getInterfaceInfo($type, int $interface_id)
    {
        $this->checkInterfaceRates();
        $index_mapping = array_flip(array_pluck($this->interfaceRatesObject, "interface_id"));
        if (!isset($index_mapping[$interface_id]))
        {
            return 0;
        }
        switch ($type)
        {
            case "rate_in":
                return $this->interfaceRatesObject[$index_mapping[$interface_id]]['rate_in'];
                break;
            case "rate_out":
                return $this->interfaceRatesObject[$index_mapping[$interface_id]]['rate_out'];
                break;
            case "rate_combined":
                return $this->interfaceRatesObject[$index_mapping[$interface_id]]['rate_combined'];
                break;
            case "time":
                return Carbon::parse($this->interfaceRatesObject[$index_mapping[$interface_id]]['time'])->addHours(2);
                break;
        }
    }
    
    public
    function getRateInForInterface($interface_id)
    {
        return $this->getInterfaceInfo("rate_in", $interface_id);
    }
    
    public
    function getRateOutForInterface($interface_id)
    {
        return $this->getInterfaceInfo("rate_out", $interface_id);
    }
    
    public
    function getRateCombinedForInterface($interface_id)
    {
        return $this->getInterfaceInfo("rate_combined", $interface_id);
    }
    
    public
    function getTimeForInterface($interface_id)
    {
        return $this->getInterfaceInfo("time", $interface_id);
    }
    
    public
    function getNameInfoAttribute()
    {
        $name = $this->name;
        
        return $name . " (" . $this->ort . ", " . $this->beschreibung . ", " . $this->kontakt . ")";
    }
    
    public
    function getNameInfoShortAttribute()
    {
        $name = $this->name;
        
        return $name . " (" . $this->ort . ", " . $this->beschreibung . ")";
    }
    
    protected static
    function boot()
    {
        parent::boot();
        
        static::deleting(function ($endpoint)
        { // before delete() method call this
            $endpoint->interfaces->each(function ($item, $key)
            {
                $item->delete();
            });
            // do the rest of the cleanup...
        });
    }
}
