<?php

namespace App;

use App\Helpers\DataRate;
use App\Helpers\SNMP\SNMP_DataChangeTracker;
use Illuminate\Database\Eloquent\Model;

class EndpointData extends Model
{
    protected $fillable     = ["endpoint_id", "mtu", "speed", "mac", "name", "index", "values"];
    private   $valuesObject = NULL;
    protected $touches      = ['endpoint'];
    
    public
    function endpoint()
    {
        return $this->belongsTo("App\SNMP_Endpoint", "endpoint_id");
    }
    
    public
    function connectedTo()
    {
        return $this->hasMany("App\EndpointConnection", "endpoint_interface_index_to", "id");
    }
    
    public
    function connectedFrom()
    {
        return $this->hasMany("App\EndpointConnection", "endpoint_interface_index_from", "id");
    }
    
    public
    function connected()
    {
        $to   = $this->connectedTo;
        $from = $this->connectedFrom;
        
        return $to->merge($from);
    }
    
    public
    function getNameInfoAttribute()
    {
        $name = $this->name;
        
        return $name . " (" . $this->index . ", " . $this->speed . ")";
    }
    
    public
    function getRateInAttribute()
    {
        $rate = optional($this->rates)->first();
        if ($rate == NULL)
        {
            return 0;
        }
        
        return $rate->in;
    }
    
    public
    function getRateOutAttribute()
    {
        $rate = optional($this->rates)->first();
        if ($rate == NULL)
        {
            return 0;
        }
        
        return $rate->out;
    }
    
    public
    function getRateCombinedAttribute()
    {
        $rate = optional($this->rates)->first();
        if ($rate == NULL)
        {
            return 0;
        }
        
        return $rate->in + $rate->out;
    }
    
    public
    function rates()
    {
        return $this->hasMany("App\snmp_perf_data", "interface_id", "id")->latest();
    }
    
    protected static
    function boot()
    {
        parent::boot();
        
        static::deleting(function ($interface)
        { // before delete() method call this
            $interface->connected->each(function ($item, $key)
            {
                $item->delete();
            });
            // do the rest of the cleanup...
        });
    }
    
}
