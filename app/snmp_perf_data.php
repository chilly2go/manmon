<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class snmp_perf_data extends Model
{
    protected $fillable = ['interface_id', 'in', 'out'];
    
    public
    function interface()
    {
        return $this->belongsTo("App\EndpointData", "interface_id");
    }
    
    public
    function scopeOld($query)
    {
        return $query->where('updated_at', '<', Carbon::now()->subHours(25));
    }
    
}
