<?php

namespace App\Console;

use App\IP_Pool;
use App\Jobs\ConvertFileCheckerJob;
use App\Jobs\ConvertNfcapdFilesToCSV;
use App\Jobs\DeleteOldIPFixMySQL;
use App\Jobs\DeleteOldSNMPMySQL;
use App\Jobs\IPLookupDispatcher;
use App\Jobs\PingDispatcher;
use App\Jobs\PingIP;
use App\Jobs\SNMPGatherInitialData;
use App\Jobs\SNMPPollData;
use App\SNMP_Endpoint;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [//
    ];
    
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected
    function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        
        // poll current snmp data
        $enabled = config("monitoring.scheduler.enabled");
        if ($enabled)
        {
            if (config("monitoring.snmp.poll.enabled", FALSE))
            {
                $schedule->call(function ()
                {
                    SNMP_Endpoint::all()->each(function ($item, $key)
                    {
                        SNMPPollData::dispatch($item)->onQueue("snmp");
                    });
                })->everyMinute();
                
                // update endpoint information (uptime and changed system info)
                $schedule->call(function ()
                {
                    SNMP_Endpoint::all()->each(function ($item, $key)
                    {
                        SNMPGatherInitialData::dispatch($item)->onQueue("snmp");
                    });
                })->everyFifteenMinutes();
                $schedule->call(function ()
                {
                    DeleteOldSNMPMySQL::dispatch();
                })->everyTenMinutes();
            }
            
            // ping (heartbeat / latency)
            if (config("monitoring.ping.enabled", FALSE))
            {
                $schedule->call(function ()
                {
                    PingDispatcher::dispatch()->onQueue("ping");
                })->everyMinute();
            }
            
            // metrics for horizon
            if (config("monitoring.horizon.statistics.enabled", FALSE))
            {
                $schedule->command('horizon:snapshot')->everyTenMinutes();
            }
            
            // check for ipfix data
            if (config("monitoring.csv.check-files.enabled", FALSE))
            {
                $schedule->call(function ()
                {
                    ConvertFileCheckerJob::dispatch()->onQueue("csv");
                    if (config("monitoring.csv.line-chunks.type") == "mysql")
                    {
                        DeleteOldIPFixMySQL::dispatch()->onQueue("csv");
                    }
                })->everyFiveMinutes();
                // using mysql for ipfix?
            }
            
            // check for IPs that require a lookup
            if (config("monitoring.lookup.check.enabled", FALSE))
            {
                $schedule->call(function ()
                {
                    IPLookupDispatcher::dispatch()->onQueue("lookup")->delay(10);
                })->everyFiveMinutes();
            }
            
        }
    }
    
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected
    function commands()
    {
        $this->load(__DIR__ . '/Commands');
        
        require base_path('routes/console.php');
    }
}
