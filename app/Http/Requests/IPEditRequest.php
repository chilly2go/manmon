<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IPEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public
    function authorize()
    {
        // no permissions and stuff, yet
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public
    function rules()
    {
        return [

            "edit_ip"     => ["required", // self explanatory
                              "ipv4",     //currently only v4

                              // small list of unwanted entries
                              Rule::notIn(['127.0.0.1',
                                           "255.255.255.255",
                                           "255.255.255.0",
                                           "255.255.0.0",
                                           "255.0.0.0"])],
            "edit_name"   => "nullable|string|min:2",
            "should_ping" => "nullable|between:0,2",

        ];
    }
}
