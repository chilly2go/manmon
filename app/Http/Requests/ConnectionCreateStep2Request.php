<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ConnectionCreateStep2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public
    function authorize()
    {
        return TRUE;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public
    function rules()
    {
        return [
            
            "from" => ["required",
                       "different:to",
                       "integer",
                       "exists:i_p__pools,id",],
            "to"   => ["required",
                       "integer",
                       "exists:i_p__pools,id",],
        
        ];
    }
}
