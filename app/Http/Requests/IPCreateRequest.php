<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IPCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public
    function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public
    function rules()
    {
        return [

            "new_ip" => ["required",
                         "ipv4",
                         "unique:i_p__pools,ip",
                         Rule::notIn(['127.0.0.1', "255.255.255.255", "255.255.255.0", "255.255.0.0", "255.0.0.0"])],

        ];
    }
}
