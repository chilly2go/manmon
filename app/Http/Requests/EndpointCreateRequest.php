<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EndpointCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public
    function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public
    function rules()
    {
        return [

            "ip"        => ["required",
                            "ipv4",
                            Rule::notIn(['127.0.0.1', "255.255.255.255", "255.255.255.0", "255.255.0.0", "255.0.0.0"])],
            "community" => "required|string|between:2,100",

        ];
    }
}
