<?php

namespace App\Http\Controllers;

use App\Helpers\ColorHelper;
use App\Helpers\DataRate;
use App\Helpers\SankeyDiagram;
use Carbon\Carbon;

class DiagramController extends Controller
{
    public
    function index()
    {
        return view("diagram.overview");
    }
    
    public
    function data()
    {
        return response()->json(['diagram' => (new \App\Helpers\OverviewDiagramData())->handle(),
                                 'updated' => "Letztes Update: " . Carbon::now()->toDateTimeString()]);
    }
    
    public
    function sankeyIPFix()
    {
        return view("diagram.sankey-ipfix");
    }
    
    public
    function sankeySNMP()
    {
        return view("diagram.sankey-snmp");
    }
    
    public
    function sankeyData(int $time, string $type)
    {
        $return = NULL;
        switch ($type)
        {
            case "ipfix":
                $return = SankeyDiagram::getIPFixData($time);
                break;
            case "snmp":
                $return = SankeyDiagram::getSNMPData($time);
                break;
        }
        
        return response()->json(['diagram' => $return,
                                 'updated' => "Letztes Update: " . Carbon::now()->toDateTimeString()]);
    }
}
