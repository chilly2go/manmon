<?php

namespace App\Http\Controllers;

use App\Http\Requests\IPCreateRequest;
use App\Http\Requests\IPEditRequest;
use App\IP_Pool;
use Illuminate\Http\Request;
use Illuminate\Support\ViewErrorBag;

class IPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function index()
    {
        //session()->forget("status");
        return view("ip.index", ["ips" => IP_Pool::with("endpoint")->orderBy("should_ping", 'DESC')->orderBy("ip")
                                                 ->paginate(config("paginate.paginate.items"))]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public
    function store(IPCreateRequest $request)
    {
        $value = $request->validated();
        IP_Pool::create(['ip' => $value['new_ip']]);
        return redirect()->route("ip.index")->with("status", "Neue IP (" . $value['new_ip'] . ") angelegt");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IP_Pool $iP_Pool
     *
     * @return \Illuminate\Http\Response
     */
    public
    function show(IP_Pool $ip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IP_Pool $iP_Pool
     *
     * @return \Illuminate\Http\Response
     */
    public
    function edit(IP_Pool $ip)
    {
        return view("ip.edit", ["ip" => $ip]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\IP_Pool             $iP_Pool
     *
     * @return \Illuminate\Http\Response
     */
    public
    function update(IPEditRequest $request, IP_Pool $ip)
    {
        $validated = $request->validated();
        $found     = IP_Pool::where("ip", "=", $validated['edit_ip'])->get();
        if (optional($found)->count() > 1 || (optional($found)->count() == 1 && $found->first()->id != $ip->id))
        {
            return back()->withInput()->with("errors", "IP wird bereits verwendet!");
        } else
        {
            $ip->ip          = $validated['edit_ip'];
            $ip->name        = $validated['edit_name'];
            $ip->should_ping = $validated['should_ping'];
            //dd($request, $validated, $ip);
            if ($ip->isDirty())
            {
                $ip->save();
                return redirect()->route("ip.edit", ["ip" => $ip->id])->with("status", "Daten erfolgreich gespeichert");
            }
            return redirect()->route("ip.edit", ["ip" => $ip->id]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IP_Pool $iP_Pool
     *
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(IP_Pool $ip)
    {
        $_ip = $ip->ip;
        $ip->delete();
        return back()->with("status", "IP (" . $_ip . ") has been deleted.");
    }
}
