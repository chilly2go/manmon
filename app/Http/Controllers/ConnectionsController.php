<?php

namespace App\Http\Controllers;

use App\EndpointConnection;
use App\EndpointData;
use App\Http\Requests\ConnectionCreateStep2Request;
use App\Http\Requests\ConnectionCreateStep3Request;
use App\Http\Requests\ConnectionEndpointRequest;
use App\Http\Requests\ConnectionInterfaceRequest;
use App\Http\Requests\ConnectionIPRequest;
use App\IP_Pool;
use App\SNMP_Endpoint;
use Illuminate\Http\Request;

class ConnectionsController extends Controller
{
    public
    function index()
    {
        $connections =
            EndpointConnection::with(['fromEndpointInterface', 'toEndpointInterface', 'toIP', 'fromIP'])->paginate();
        
        return view('connections.index', ['connections' => $connections]);
    }
    
    public
    function create_step1()
    {
        $ips = IP_Pool::privateAddress()->orWhere('should_ping', '>', 0)->orderBy('name')->orderBy('ip')->get();
        
        return view('connections.create-step1', ['ips' => $ips,]);
    }
    
    public
    function create_step2(ConnectionCreateStep2Request $request)
    {
        $val  = $request->validated();
        $from = IP_Pool::find($val['from'])->load('endpoint.interfaces');
        $to   = IP_Pool::find($val['to'])->load('endpoint.interfaces');
        if ($from->endpoint == NULL && $to->endpoint == NULL)
        {
            EndpointConnection::firstOrCreate(['ip_from_id' => $val['from'], 'ip_to_id' => $val['to']]);
            
            return redirect()->route("connection.index")->with("status", "Neue Verbindung erfolgreich gespeichert!");
        }
        
        return view('connections.create-step2', ['values' => $val, 'from' => $from, 'to' => $to]);
    }
    
    public
    function create_step3(ConnectionCreateStep3Request $request)
    {
        $val            = $request->validated();
        $to_interface   = $val['to_interface'] ?? NULL;
        $from_interface = $val['from_interface'] ?? NULL;
        EndpointConnection::firstOrCreate(['ip_from_id'                    => $val['from'],
                                           'ip_to_id'                      => $val['to'],
                                           'endpoint_interface_index_from' => $from_interface,
                                           'endpoint_interface_index_to'   => $to_interface]);
        
        return redirect()->route("connection.index")->with("status", "Neue Verbindung erfolgreich gespeichert!");
    }
    
    public
    function delete(EndpointConnection $connection)
    {
        $connection->delete();
        
        return redirect()->route("connection.index")->with("status", "Verbindung gelöscht!");
    }
}
