<?php

namespace App\Http\Controllers;

use App\Helpers\SNMP\OID;
use App\Helpers\SNMP\SNMP;
use App\Helpers\SNMP\SNMP_Adress;
use App\Helpers\SNMP\SNMP_AdressConversion;
use App\IP_Pool;
use App\Jobs\SNMPGatherInitialData;
use App\SNMP_Endpoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShowHome extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public
    function __invoke(Request $request)
    {
        return view("layouts.home");
    }
}
