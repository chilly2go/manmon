<?php

namespace App\Http\Controllers;

use App\Helpers\InfluxDataInterface;
use App\Http\Requests\EndpointCreateRequest;
use App\Http\Requests\EndpointEditRequest;
use App\Http\Requests\IPCreateRequest;
use App\Http\Requests\IPEditRequest;
use App\IP_Pool;
use App\Jobs\SNMPGatherInitialData;
use App\SNMP_Endpoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ViewErrorBag;
use InfluxDB\Client;

class EndpointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function index()
    {
        //session()->forget("status");
        return view("endpoint.index",
                    ["endpoints" => SNMP_Endpoint::with(["ip", "interfaces"])
                                                 ->orderBy("name")
                                                 ->paginate(config("paginate.paginate.items"))]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public
    function store(EndpointCreateRequest $request)
    {
        $value  = $request->validated();
        $ip     = IP_Pool::firstOrCreate(['ip' => $value['ip']]);
        $ep     = SNMP_Endpoint::firstOrCreate(['ip_id' => $ip->id, 'community' => $value['community']]);
        $status = "";
        if ($ep->wasRecentlyCreated)
        {
            $status = "Neues Endgerät mit IP (" . $value['ip'] . ") angelegt";
            SNMPGatherInitialData::dispatch($ep)->onQueue("snmp");
        } else
        {
            $status = "Endgerät (IP: " . $ip->ip . ", Name: " . $ep->name . " existiert bereits";
        }
        
        return redirect()->route("endpoint.index")->with("status", $status);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SNMP_Endpoint $SNMP_Endpoint
     *
     * @return \Illuminate\Http\Response
     */
    public
    function edit(SNMP_Endpoint $endpoint)
    {
        
        $endpoint->load('interfaces.rates');
        
        return view("endpoint.edit", ["endpoint" => $endpoint]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\SNMP_Endpoint       $SNMP_Endpoint
     *
     * @return \Illuminate\Http\Response
     */
    public
    function update(EndpointEditRequest $request, SNMP_Endpoint $endpoint)
    {
        $validated = $request->validated();
        // $found     = SNMP_Endpoint::where("ip", "=", $validated['edit_ip'])->get();
        $endpoint->name      = $validated['name'];
        $endpoint->community = $validated['community'];
        if ($endpoint->isDirty())
        {
            $endpoint->save();
            
            return redirect()
                ->route("endpoint.edit", ["endpoint" => $endpoint->id])
                ->with("status", "Daten erfolgreich gespeichert");
        }
        
        return redirect()->route("endpoint.edit", ["endpoint" => $endpoint->id]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SNMP_Endpoint $SNMP_Endpoint
     *
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(SNMP_Endpoint $endpoint)
    {
        if ($endpoint != NULL)
        {
            $name = $endpoint->name;
            $endpoint->interfaces()->delete();
            $endpoint->delete();
            
            return back()->with("status", "Endgerät (" . $name . ") wurde gelöscht.");
        } else
        {
            return back()->with("error", "Endgerät mit dieser Kennung existiert nicht.");
        }
    }
}
