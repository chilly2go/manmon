<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndpointConnection extends Model
{
    protected $fillable = ["ip_from_id", "ip_to_id", "endpoint_interface_index_from", "endpoint_interface_index_to"];
    
    public
    function fromIP()
    {
        return $this->belongsTo("App\IP_Pool", "ip_from_id", "id");
    }
    
    public
    function toIP()
    {
        return $this->belongsTo("App\IP_Pool", "ip_to_id", "id");
    }
    
    public
    function fromEndpointInterface()
    {
        return $this->belongsTo("App\EndpointData", "endpoint_interface_index_from");
    }
    
    public
    function toEndpointInterface()
    {
        return $this->belongsTo("App\EndpointData", "endpoint_interface_index_to");
    }
    
}
