<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 13.09.2018
 * Time: 16:03
 */

namespace App\Helpers;

use App\EndpointConnection;
use App\SNMP_Endpoint;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SankeyDiagram
{
    public static
    function getSNMPData(int $minutes)
    {
        $endpoints     = SNMP_Endpoint::all()->load(['ip',
                                                     'interfaces.connectedFrom.fromIP',
                                                     'interfaces.connectedFrom.toIP',
                                                     'interfaces.connectedTo.fromIP',
                                                     'interfaces.connectedTo.toIP']);
        $colorHelper   = new ColorHelper();
        $dataArray     = array();
        $linkDataArray = array();
        $time          = Carbon::now()->subMinutes($minutes)->toDateTimeString();
        $filter        = array();
        $dataArray[0]  = ["key"   => 0,
                          "text"  => "Nicht zugewiesen",
                          "color" => $colorHelper->getColor()];
        foreach ($endpoints as $endpoint)
        {
            $rate_sum = 0;
            foreach ($endpoint->interfaces as $interface)
            {
                $rate = 0;
                \DB::connection()->enableQueryLog();
                $rates = $interface->rates()->where('created_at', '>=', $time)->get();
                if ($rates->count() > 0)
                {
                    $rate = $rates->sum('in') + $rates->sum('out');
                }
                $count = $interface->connectedFrom->count() + $interface->connectedTo->count();
                if ($count > 0)
                {
                    $rate     = round($rate / $count, 2);
                    $rate_sum += $rate;
                    foreach ($interface->connectedFrom as $connection)
                    {
                        
                        if (!isset($filter[$connection->id]))
                        {
                            $linkDataArray[]         = ['from'  => $connection->fromIP->id,
                                                        'to'    => $connection->toIP->id,
                                                        'width' => $rate];
                            $filter[$connection->id] = $rate;
                            if (!isset($dataArray[$connection->toIP->id]))
                            {
                                if ($connection->toIP->endpoint != NULL)
                                {
                                    $dataArray[$connection->toIP->id] = ["key"   => $connection->toIP->id,
                                                                         "text"  => $connection->toIP->endpoint->name .
                                                                                    " (" . $connection->toIP->ip . ")",
                                                                         "color" => $colorHelper->getColor()];
                                } else
                                {
                                    $dataArray[$connection->toIP->id] = ["key"   => $connection->toIP->id,
                                                                         "text"  => $connection->toIP->nameInfo . " " .
                                                                                    DataRate::calc($rate),
                                                                         "color" => $colorHelper->getColor()];
                                }
                            }
                        }
                    }
                    foreach ($interface->connectedTo as $connection)
                    {
                        if (!isset($filter[$connection->id]))
                        {
                            $linkDataArray[]         = ['from'  => $connection->fromIP->id,
                                                        'to'    => $connection->toIP->id,
                                                        'width' => $rate];
                            $filter[$connection->id] = $rate;
                            if (!isset($dataArray[$connection->fromIP->id]))
                            {
                                if ($connection->fromIP->endpoint != NULL)
                                {
                                    $dataArray[$connection->fromIP->id] = ["key"   => $connection->fromIP->id,
                                                                           "text"  => $connection->fromIP->endpoint->name .
                                                                                      " (" . $connection->fromIP->ip .
                                                                                      ")",
                                                                           "color" => $colorHelper->getColor()];
                                } else
                                {
                                    $dataArray[$connection->fromIP->id] = ["key"   => $connection->fromIP->id,
                                                                           "text"  => $connection->fromIP->nameInfo .
                                                                                      " " . DataRate::calc($rate),
                                                                           "color" => $colorHelper->getColor()];
                                }
                            }
                        }
                    }
                } else
                {
                    if (config("monitoring.diagram.snmp.show_unknown", FALSE) && $rates->count() > 0)
                    {
                        // dd($interface->load("endpoint"));
                        $linkDataArray[] =
                            ['from' => $endpoint->ip->id, 'to' => 0, 'width' => $rate, "text" => $interface->name];
                    }
                }
            }
            $dataArray[$endpoint->ip->id] = ["key"   => $endpoint->ip->id,
                                             "text"  => $endpoint->name . " (" . $endpoint->ip->ip . ") " .
                                                        DataRate::calc($rate_sum),
                                             "color" => $colorHelper->getColor()];
        }
        $tmp = collect($linkDataArray);
        $min = $tmp->min("width");
        $max = $tmp->max("width");
        foreach ($linkDataArray as $key => $val)
        {
            $linkDataArray[$key]['width'] = round((($val['width'] - $min) / ($max - $min) + 0.05) * 40, 2);
        }
        
        return ["class"         => "go.GraphLinksModel",
                "nodeDataArray" => array_values($dataArray),
                "linkDataArray" => $linkDataArray,];
    }
    
    public static
    function getIPFixData(int $minutes)
    {
        $data          = DB::select("select trim(both '\"' FROM
            coalesce(json_extract(ip_from.data, '$.org'), json_extract(ip_from.data, '$.combined'), snmp_from.name,
                     ip_from.name, ip_from.ip))                                    as from_name,
       trim(both '\"' FROM
            coalesce(json_extract(ip_to.data, '$.org'), json_extract(ip_to.data, '$.combined'), snmp_to.name,
                     ip_to.name, json_extract(ip_to.data, '$.message'), ip_to.ip)) as to_name,
       sum(fix.bytes_in)                                                           as bytes_in,
       sum(fix.bytes_out)                                                           as bytes_out,
       greatest(1, round(sum(fix.duration) / 1000, 2))                           as duration
from ipfix fix
       join i_p__pools ip_from on fix.from_ip = ip_from.id
       left join s_n_m_p__endpoints snmp_from on ip_from.id = snmp_from.ip_id
       join i_p__pools ip_to on fix.to_ip = ip_to.id
       left join s_n_m_p__endpoints snmp_to on ip_to.id = snmp_to.ip_id
where fix.start > :starttime
  and ip_from.ip like '192.168.178.%' and fix.from_ip != fix.to_ip
group by fix.from_ip, to_name;",
                                    [// ":minutes"   => $minutes,
                                     ":starttime" => Carbon::now()->subMinutes($minutes)->toDateTimeString()]);
        $dataArray     = array();
        $linkDataArray = array();
        $dataRate      = array();
        $colorHelper   = new ColorHelper();
        foreach ($data as $point)
        {
            if (!isset($dataArray[$point->from_name]))
            {
                $dataArray[$point->from_name] = ["key"   => str_replace(["\"", "\\\""], ["", ""], $point->from_name),
                                                 "text"  => str_replace(["\"", "\\\""], ["", ""], $point->from_name),
                                                 "color" => $colorHelper->getColor()];
            }
            if (!isset($dataArray[$point->to_name]))
            {
                $dataArray[$point->to_name] = ["key"   => str_replace(["\"", "\\\""], ["", ""], $point->to_name),
                                               "text"  => str_replace(["\"", "\\\""], ["", ""], $point->to_name),
                                               "color" => $colorHelper->getColor()];
            }
            $in                                   = $point->bytes_in / $point->duration;
            $out                                  = $point->bytes_out / $point->duration;
            $dataRate[$point->from_name]["in"][]  = $out;
            $dataRate[$point->from_name]["out"][] = $in;
            $dataRate[$point->to_name]["in"][]    = $in;
            $dataRate[$point->to_name]["out"][]   = $out;
            $linkDataArray[]                      = ['from'  => $point->from_name,
                                                     'to'    => $point->to_name,
                                                     'width' => $in + $out];
        }
        $tmp = collect($linkDataArray);
        $min = $tmp->min("width");
        $max = $tmp->max("width");
        for ($i = 0; $i < count($linkDataArray); $i++)
        {
            $linkDataArray[$i]['width'] = round((($linkDataArray[$i]['width'] - $min) / ($max - $min) + 0.05) * 70, 2);
        }
        foreach ($dataRate as $name => $rates)
        {
            // $dataArray[$name]['text'] .= " In: " .
            //                              DataRate::calc(isset($rates['in']) ? collect($rates['in'])->sum() * 1024 : 0) .
            //                              " Out: " .
            //                              DataRate::calc(isset($rates['out']) ? collect($rates['out'])->sum() * 1024 :
            //                                                 0);
            $dataArray[$name]['text'] .= " In: " .
                                         DataRate::calc(isset($rates['in']) ? collect($rates['in'])->sum() : 0) .
                                         " Out: " .
                                         DataRate::calc(isset($rates['out']) ? collect($rates['out'])->sum() : 0);
        }
        
        // dd(array_values($dataArray), $linkDataArray, $data);
        
        return ["class"         => "go.GraphLinksModel",
                "nodeDataArray" => array_values($dataArray),
                "linkDataArray" => $linkDataArray,];
    }
}