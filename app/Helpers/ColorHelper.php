<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 13.09.2018
 * Time: 12:40
 */

namespace App\Helpers;

use Mexitek\PHPColors\Color;

class ColorHelper
{
    private $colors;
    private $ctr;
    
    public
    function __construct($colors = NULL)
    {
        if ($colors == NULL)
        {
            $this->colors = array("#e6194B",
                                  "#3cb44b",
                                  "#ffe119",
                                  "#4363d8",
                                  "#f58231",
                                  "#911eb4",
                                  "#42d4f4",
                                  "#f032e6",
                                  "#bfef45",
                                  "#fabebe",
                                  "#469990",
                                  "#e6beff",
                                  "#9A6324",
                                  "#fffac8",
                                  "#800000",
                                  "#aaffc3",
                                  "#808000",
                                  "#ffd8b1",
                                  "#000075",
                                  "#a9a9a9");
        } else $this->colors = $colors;
        $this->ctr = 0;
    }
    
    public
    function getColor()
    {
        $index = floor($this->ctr / 5) % count($this->colors);
        $color = new Color($this->colors[$index]);
        switch ($this->ctr % 5)
        {
            case 1:
                $colorVal = $color->lighten(15);
                break;
            case 2:
                $colorVal = $color->darken(15);
                break;
            case 3:
                $colorVal = $color->darken(30);
                break;
            case 4:
                $colorVal = $color->darken(45);
                break;
            default:
                $colorVal = $color->getHex();
        }
        $this->ctr++;
        
        return "#" . $colorVal;
    }
}