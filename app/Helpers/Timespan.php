<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 22.08.2018
 * Time: 22:03
 */

namespace App\Helpers;


class Timespan
{
    private static
    function calc(int $seconds)
    {
        $years = 0;
        $days  = $seconds / 8640000;
        if ($days > 365)
        {
            $years = intval($days / 365);
            $days  = $days % 365;
        }
        $hours   = ($days - intval($days)) * 24;
        $minutes = ($hours - intval($hours)) * 60;
        $seconds = ($minutes - intval($minutes)) * 60;
        return [$years, $days, $hours, $minutes, $seconds];
    }

    public static
    function timespan(int $seconds)
    {
        list($years, $days, $hours, $minutes, $seconds) = static::calc($seconds);
        return $years > 0 ? intval($years) . " Jahre," :
            "" . " " . intval($days) . " Tage, " . intval($hours) . " Stunden, " . intval($minutes) . " Minuten, " .
            intval($seconds) . " Sekunden";
    }

    public static
    function timespanHtml(int $seconds)
    {
        list($years, $days, $hours, $minutes, $seconds) = static::calc($seconds);
        $ret = "<div class=\"timespan\">";
        if ($years > 0)
        {
            $ret .= "<span>" . $years . " Jahre, </span>";
        }
        $ret .= "<span>" . str_pad(intval($days), 2, "0", STR_PAD_LEFT) . " Tage, </span>";
        $ret .= "<span>" . str_pad(intval($hours), 2, "0", STR_PAD_LEFT) . " Stunden, </span>";
        $ret .= "<span>" . str_pad(intval($minutes), 2, "0", STR_PAD_LEFT) . " Minuten, </span>";
        $ret .= "<span>" . intval($seconds) . " Sekunden</span>";
        $ret .= "</div>";
        return $ret;
    }
}