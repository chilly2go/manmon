<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 23.08.2018
 * Time: 00:00
 */

namespace App\Helpers;

class DataRate
{
    
    private const THRESHOLD = 2048;
    private const UNIT_STEP = 1024;
    
    public static
    function calc($valuesInbound, $increase = 1)
    {
        // we want byte/s :)
        $valuesInbound /= 8;
        $unit          = "B/s";
        if ($valuesInbound > self::THRESHOLD || $increase > 1)
        {
            if ($increase > 1)
            {
                $increase--;
            } else
            {
                $valuesInbound /= self::UNIT_STEP;
            }
            $unit = "KB/s";
            if ($valuesInbound > self::THRESHOLD || $increase > 1)
            {
                if ($increase > 1)
                {
                    $increase--;
                } else
                {
                    $valuesInbound /= self::UNIT_STEP;
                }
                $unit = "MB/s";
                if ($valuesInbound > self::THRESHOLD || $increase > 1)
                {
                    if ($increase > 1)
                    {
                        $increase--;
                    } else
                    {
                        $valuesInbound /= self::UNIT_STEP;
                    }
                    $unit = "GB/s";
                    if ($valuesInbound > self::THRESHOLD || $increase > 1)
                    {
                        if ($increase > 1)
                        {
                            $increase--;
                        } else
                        {
                            $valuesInbound /= self::UNIT_STEP;
                        }
                        $unit = "TB/s";
                    }
                }
            }
        }
        $value = number_format($valuesInbound, 1, ",", ".");
        
        return str_pad($value, 4, "0", STR_PAD_LEFT) . " " . $unit;
    }
}