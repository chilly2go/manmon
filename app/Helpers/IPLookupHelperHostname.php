<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 */

namespace App\Helpers;


use App\IP_Pool;

class IPLookupHelperHostname
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public static
    function handle($ip_in)
    {
        if (!($ip_in instanceof IP_Pool))
        {
            $ip = IP_Pool::where('ip', $ip_in)->first();
        } else
        {
            $ip = $ip_in;
        }
        if ($ip == NULL)
        {
            throw new \Exception("Can't find IP-instance of " . $ip_in);
        }
        $command = "nmblookup -A " . $ip->ip;
        $output  = shell_exec($command);
        $data    = explode("\n", $output);
        $return  = array();
        $error   = array();
        foreach ($data as $line)
        {
            $line = str_replace("\t", "", $line);
            if (str_contains($line, "<00>"))
            {
                array_push($return, explode(" ", $line)[0]);
            } else
            {
                if (str_contains($line, "No reply"))
                {
                    $error = ["No-Reply" => $ip->ip];
                }
            }
        }
        if (isset($return[0], $return[1]))
        {
            $ip->data = json_encode(['hostname'  => $return[0],
                                     'groupname' => $return[1] ?? "",
                                     'combined'  => implode(".", $return)]);
        } else
        {
            if (isset($ip->data, get_object_vars(json_decode($ip->data))['hostname']))
            {
                $ip->data = json_encode((json_decode($ip->data)->recent = "Überprüfung fehlgeschlagen. Keine Antwort"));
            } else
            {
                $ip->data = json_encode(["could-not-match" => $data]);
            }
        }
        $ip->save();
    }
}