<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 09.09.2018
 * Time: 15:30
 */

namespace App\Helpers\IPFix;

use App\Helpers\InfluxDataInterface;
use Carbon\Carbon;

class LineHandler
{
    
    public static
    function handle($line)
    {
        if (count($line) < 14)
        {
            return 0;
        }
        if ($line[0] == "Summary")
        {
            return -1;
        }
        $return = ['start'            => Carbon::parse($line[0]),
                   'end'              => Carbon::parse($line[1]),
                   'duration'         => $line[2],
                   'from_ip'          => $line[3],
                   'from_port'        => $line[5],
                   'to_ip'            => $line[4],
                   'to_port'          => $line[6],
                   'protocol'         => $line[7],
                   'flags'            => $line[8],
                   'packet_count_in'  => $line[11],
                   'packet_bytes_in'  => $line[12],
                   'packet_count_out' => $line[13],
                   'packet_bytes_out' => $line[14],];
        
        return $return;
    }
    
    public static
    function handleIPsOnly($line)
    {
        if (count($line) < 12)
        {
            return 0;
        }
        if ($line[0] == "Summary")
        {
            return -1;
        }
        $return = [
            
            'from_ip' => $line[3],
            'to_ip'   => $line[4],];
        
        return $return;
    }
}