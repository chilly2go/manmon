<?php
/**
 * Little wrapper for influxdb-client to automatically establish the appropriate connection and help with writing data.
 * For selecting data the already available querybuilder is much more helpful
 *
 * @author  chilly
 * @version 0.1.2
 * @since   01.08.2018 Class to establish InfluxDB Connection (settings taken from config)
 * @since   31.08.2018 Made Access mostly static to ease usage
 * @example Examples can be found at https://github.com/influxdata/influxdb-php
 */

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use InfluxDB\Client;
use InfluxDB\Database;

class InfluxDataInterface
{
    public $database;

    public
    function __construct()
    {
        $this->database = InfluxDataInterface::establishConnection();
    }

    private static
    function establishConnection()
    {
        try
        {
            return Client::fromDSN(sprintf('influxdb://user:pass@%s:%s/%s', config("influx.host"),
                                           config("influx.port"), config("influx.dbname")), 5);
        } catch (Client\Exception $e)
        {
            Log::channel("influx")->error($e->getMessage());
            return NULL;
        }
    }

    public static
    function getDatabase()
    {
        return InfluxDataInterface::establishConnection();
    }

    public
    function getConnection()
    {
        return $this->database;
    }

    public static
    function write($points)
    {
        $data = array();
        if (!is_array($points))
        {
            $data[] = $points;
        } else
        {
            $data = $points;
        }

        InfluxDataInterface::establishConnection()->writePoints($data, Database::PRECISION_SECONDS);
    }
}