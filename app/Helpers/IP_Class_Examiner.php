<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 22:29
 */

namespace App\Helpers;


class IP_Class_Examiner
{
    public const PUBLIC       = 1;
    public const PRIVATE      = 2;
    public const MULTICAST    = 3;
    public const EXPERIMENTAL = 4;

    public static
    function getIPv4Class(string $ip)
    {
        $parts  = explode(".", $ip);
        $first  = intval($parts[0]);
        $second = intval($parts[1]);
        //probably public
        if ($first == 10 || $first == 172 && ($second >= 16 && $second <= 31) || ($first == 192 && $second == 168))
        {
            return IP_Class_Examiner::PRIVATE;
        }
        if ($first >= 224 && $first <= 239)
        {
            return IP_Class_Examiner::MULTICAST;
        }
        if ($first >= 240)
        {
            return IP_Class_Examiner::EXPERIMENTAL;
        }
        return IP_Class_Examiner::PUBLIC;
    }
}