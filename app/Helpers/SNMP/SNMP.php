<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 31.07.2018
 * Time: 18:39
 */

namespace App\Helpers\SNMP;

use Illuminate\Support\Facades\Log;

class SNMP
{

    const SNMP_OUTPUT       = SNMP_OID_OUTPUT_NUMERIC;
    const SNMP_VALUE_FORMAT = SNMP_VALUE_PLAIN;
    const SNMP_VERSION      = 2;
    /**
     * @var String Hostname to query
     */
    private $hostname;
    /**
     * @var String SNMP Community
     */
    private $community;


    /**
     * SNMP constructor.
     *
     * @param String $hostname
     * @param String $community
     */
    public
    function __construct(String $hostname, String $community)
    {
        $this->hostname  = $hostname;
        $this->community = $community;
        // Do not (try) to translate numeric oid to name (iso.org.dod....)
        snmp_set_oid_numeric_print(SNMP::SNMP_OUTPUT);
        // Don't let the SNMP library get cute with value interpretation.  This makes
        // MAC addresses return the 6 binary bytes, timeticks to return just the integer
        // value, and some other things.
        snmp_set_valueretrieval(SNMP::SNMP_VALUE_FORMAT);
    }

    /**
     * @param      $oid            String oid or array of oids
     * @param bool $filterOIDIndex should the result contain the full oid or just the relevant index?
     * @param bool $preserveKeys   should the array keys of <b>$oid</b> be preserved. (only if array)
     *
     * @return array|string
     */
    public
    function get($oid, bool $filterOIDIndex = TRUE, bool $preserveKeys = TRUE)
    {
        if (is_array($oid))
        {
            $ret = array();
            foreach ($oid as $key => $val)
            {
                if ($preserveKeys)
                {
                    $ret[$key] = $this->get($val);
                } else
                {
                    $ret[] = $this->get($val);
                }
            }
            return $ret;
        } else
        {
            $ret = "";
            try
            {
                $ret = snmp2_get($this->hostname, $this->community, $oid);
            } catch (\ErrorException $e)
            {
                //dd($e);
                Log::channel("snmperrors")->error("get: " . $e->getMessage());
                $ret = [$oid => NULL];
            }
            if ($filterOIDIndex)
            {
                return $this->filterOIDIndex($ret);
            } else
            {
                return $ret;
            }
        }
    }

    /**
     * @param array|string $oid            String oid or array of oids
     * @param bool         $filterOIDIndex should the result contain the full oid or just the relevant index?
     * @param bool         $preserveKeys   should the array keys of <b>$oid</b> be preserved. (only if array)
     *
     * @return array|string
     */
    public
    function walk($oid, bool $filterOIDIndex = TRUE, bool $preserveKeys = TRUE)
    {
        if (is_array($oid))
        {
            $ret = array();
            foreach ($oid as $key => $val)
            {
                if ($preserveKeys)
                {
                    $ret[$key] = $this->walk($val);
                } else
                {
                    $ret[] = $this->walk($val);
                }
            }
            return $ret;
        } else
        {
            $ret = "";
            try
            {
                $ret = snmp2_real_walk($this->hostname, $this->community, $oid);
            } catch (\ErrorException $e)
            {
                Log::channel("snmperrors")->error("walk: " . $e->getMessage());
                $ret = [$oid => NULL];
            }
            if ($filterOIDIndex)
            {
                return $this->filterOIDIndex($ret);
            } else
            {
                return $ret;
            }
        }
    }

    /**
     * Filters oid-keys and returns only the relevant index as key (easier to get a specific value)
     *
     * @param array $toFilter
     *
     * @return array
     */
    private
    function filterOIDIndex(array $toFilter)
    {
        $ret = array();
        foreach ($toFilter as $key => $val)
        {
            if (is_array($val))
            {
                $ret[$key] = $this->filterOIDIndex($val);
            } else
            {
                $keyparts            = explode(".", $key);
                $ret[end($keyparts)] = $val;
            }
        }
        return $ret;
    }
}