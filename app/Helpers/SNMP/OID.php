<?php
/**
 * Get OIDs for various parts of info to query snmp endpoint
 * User: cookie
 * Date: 01.08.2018
 * Time: 11:01
 */

namespace App\Helpers\SNMP;


class OID
{
    public static
    function getData()
    {
        return array(

            "portstatus"  => ".1.3.6.1.2.1.2.2.1.7",
            "linkstatus"  => ".1.3.6.1.2.1.2.2.1.8",
            "lastchange"  => ".1.3.6.1.2.1.2.2.1.9",
            "inoctets"    => ".1.3.6.1.2.1.2.2.1.10",
            // "indiscards"  => ".1.3.6.1.2.1.2.2.1.13",
            "inerrors"    => ".1.3.6.1.2.1.2.2.1.14",
            "outoctets"   => ".1.3.6.1.2.1.2.2.1.16",
            // "outdiscards" => ".1.3.6.1.2.1.2.2.1.19",
            "outerrors"   => ".1.3.6.1.2.1.2.2.1.20",

        );
    }

    public static
    function getInterfaceInfo()
    {
        return array(

            'index'       => ".1.3.6.1.2.1.2.2.1.1",
            "description" => ".1.3.6.1.2.1.2.2.1.2",
            "mtu"         => ".1.3.6.1.2.1.2.2.1.4",
            "speed"       => ".1.3.6.1.2.1.2.2.1.5",
            "address"     => ".1.3.6.1.2.1.2.2.1.6",

        );
    }

    public static
    function getEndpointInfo()
    {
        return array(

            "descr"    => ".1.3.6.1.2.1.1.1",
            "uptime"   => ".1.3.6.1.2.1.1.3",
            "contact"  => ".1.3.6.1.2.1.1.4",
            "name"     => ".1.3.6.1.2.1.1.5",
            "location" => ".1.3.6.1.2.1.1.6",

        );
    }
}