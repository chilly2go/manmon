<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 31.07.2018
 * Time: 22:13
 */

namespace App\Helpers\SNMP;


class SNMP_AdressConversion
{
    public static
    function dechex($input)
    {
        if (is_array($input))
        {
            $ret = array();
            foreach ($input as $key => $value)
            {
                $ret[$key] = SNMP_AdressConversion::dechex($value);
            }
            return $ret;
        } else
        {
            return str_pad(dechex($input), 2, "0", STR_PAD_LEFT);
        }
    }

    public static
    function hexdec($input)
    {
        if (is_array($input))
        {
            $ret = array();
            foreach ($input as $key => $value)
            {
                $ret[$key] = SNMP_AdressConversion::hexdec($value);
            }
            return $ret;
        } else
        {
            return hexdec($input);
        }
    }
}