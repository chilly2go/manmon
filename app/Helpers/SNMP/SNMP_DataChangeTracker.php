<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 01.08.2018
 * Time: 19:35
 */

namespace App\Helpers\SNMP;

use App\EndpointData;
use App\snmp_perf_data;
use Carbon\Carbon;
use InfluxDB\Point;

class SNMP_DataChangeTracker
{
    const SNMP_COUNTER_MAX = 4294967295;
    const SNMP_INT_Min     = -2147483647;
    /**
     * @var EndpointData
     */
    private $interface;
    private $timestamp;
    private $values_old;
    private $values_new;
    private $endpoint_id;
    
    public
    function __construct(EndpointData $interface, $values_old, $values_new, $endpoint_id)
    {
        $this->interface   = $interface;
        $this->timestamp   = Carbon::now(+2);
        $this->values_old  = $values_old;
        $this->values_new  = $values_new;
        $this->endpoint_id = $endpoint_id;
    }
    
    private
    function getTags()
    {
        return [
            
            // "beschreibung"    => $this->interface->endpoint->beschreibung,
            // "ort"             => $this->interface->endpoint->ort,
            // "name"            => $this->interface->endpoint->name,
            // "kontakt"         => $this->interface->endpoint->kontakt,
            "endpoint_id"     => $this->endpoint_id,
            "interface_id"    => $this->interface->id,
            "interface_index" => $this->interface->index,
            // "interface_name"  => $this->interface->name,
            // "interface_mac"   => $this->interface->mac,
            // "interface_mtu"   => $this->interface->mtu,
            // "interface_speed" => $this->interface->speed,
        
        ];
    }
    
    private
    function extractRates($diffs)
    {
        $diff = $this->interface->updated_at->diffInSeconds($this->timestamp);
        if ($diff == 0 || $diff == NULL)
        {
            \Log::info("[SNMP_DataChangeTracker]: Div 0: " . $diff . "|" . $this->timestamp->toDateTimeString() . "|" .
                       $this->interface->updated_at->toDateTimeString());
            $diff = 1;
        }
        $rate['rate_in']       = (int)($diffs['inoctets'] / $diff);
        $rate['rate_out']      = (int)($diffs['outoctets'] / $diff);
        $rate['rate_combined'] = $rate['rate_in'] + $rate['rate_out'];
        
        return $rate;
    }
    
    public
    function handleChanges()
    {
        if ($this->values_old !== NULL)
        {
            $diffs = array();
            try
            {
                foreach ($this->values_old as $key => $value)
                {
                    if (!isset($this->values_new[$key]))
                    {
                        continue;
                    }
                    $arraykey = key($this->values_new[$key]);
                    // get_object_vars transforms object fields to array
                    $val = get_object_vars($value);
                    // array_pop gives the last element of an array. as we only have one element it is also the first
                    $new_val = $this->values_new[$key][$arraykey];
                    $old_val = array_pop($val);
                    $min     = 0;
                    if ($new_val < $min)
                    {
                        \Log::error("[SNMP_DataChangeTracker][if:" . $this->interface->id .
                                    "] Counter should start at 0 on overflow but is " . $new_val);
                        $min = self::SNMP_INT_Min;
                    }
                    $diffs[$key] = (int)($old_val > $new_val ? (self::SNMP_COUNTER_MAX - $old_val) + ($min + $new_val) :
                        $new_val - $old_val);
                    if ($diffs[$key] < 0)
                    {
                        \Log::error("[SNMP_DataChangeTracker][if:" . $this->interface->id . "|endpoint:" .
                                    $this->endpoint_id . "]: " . implode(",",
                                                                         [$diffs[$key],
                                                                          $min,
                                                                          $old_val,
                                                                          $new_val,
                                                                          $new_val - $old_val,
                                                                          (self::SNMP_COUNTER_MAX - $old_val),
                                                                          self::SNMP_COUNTER_MAX]));
                    }
                }
            } catch (\Exception $e)
            {
                \Log::info("[SNMP_DataChangeTracker](" . $this->interface->name . " (" . $this->interface->index .
                           ")): Error: " . $e->getMessage() . "|" . json_encode($this->values_old) . "|" .
                           json_encode($this->values_new) . "|" . $key . "|" . $arraykey);
                dd($this->values_old, $this->values_new, $e);
                
            }
            $rates = $this->extractRates($diffs);
            if (config("monitoring.snmp.poll.datastore", "mysql") == "influx")
            {
                $diffs = array_merge($diffs, $rates);
                
                return new Point("interface_metric",
                                 $rates['rate_combined'],
                                 $this->getTags(),
                                 $rates,
                                 $this->timestamp->timestamp);
            } else
            {
                if ($rates['rate_in'] > 0 || $rates['rate_out'] > 0)
                {
                    snmp_perf_data::create(["interface_id" => $this->interface->id,
                                            "in"           => $rates['rate_in'],
                                            'out'          => $rates['rate_out']]);
                }
                
                return NULL;
            }
        }
    }
}