<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 31.07.2018
 * Time: 22:19
 */

namespace App\Helpers\SNMP;


class SNMP_Adress
{
    private $addresses;

    public
    function __construct($adresses = NULL)
    {
        if ($adresses !== NULL)
        {
            $this->handleAdresses($adresses);
        }
    }

    public
    function handleAdresses($adresses)
    {
        if (is_array($adresses))
        {
            foreach ($adresses as $adress)
            {
                $this->handleAdresses($adress);
            }
        } else
        {
            $this->addresses[] = SNMP_AdressConversion::dechex(unpack("C*", $adresses));
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public
    function getAddresses()
    {
        if (count($this->addresses) == 1)
        {
            return reset($this->addresses);
        }
        return $this->addresses;
    }

    public
    function getAdressesMAC()
    {
        if (is_array($this->addresses))
        {
            $ret = array();
            foreach ($this->addresses as $key => $address)
            {
                $ret[$key] = implode(":", $address);
            }
            return $ret;
        } else
        {
            return implode(":", $this->addresses);
        }
    }
}