<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 13.09.2018
 * Time: 21:46
 */

namespace App\Helpers;

use App\EndpointConnection;

class OverviewDiagramData
{
    
    public
    function handle()
    {
        $nodeDataArray[0] = config("monitoring.diagram.overview.dummy-entry.nodedata");
        $linkDataArray    = [];
        $connections      = EndpointConnection::with(["toIP",
                                                      "fromIP",
                                                      "fromEndpointInterface.endpoint",
                                                      "fromEndpointInterface.rates",
                                                      "toEndpointInterface.endpoint"])->orderBy("id")->get();
        $linkDataArray    = config("monitoring.diagram.overview.dummy-entry.linkdata");
        $filter           = [];
        
        foreach ($connections as $connection)
        {
            if (!isset($nodeDataArray[$connection->ip_from_id]))
            {
                $nodeDataArray[$connection->ip_from_id] =
                    ["key" => $connection->ip_from_id, "text" => $connection->fromIP->treeName];
            }
            if (!isset($nodeDataArray[$connection->ip_to_id]))
            {
                $nodeDataArray[$connection->ip_to_id] =
                    ["key" => $connection->ip_to_id, "text" => $connection->toIP->treeName];
            }
            // reversed order to force diagram layout (top->down => external->internal
            $text = "";
            // rates are collected every 5 minutes and ordered desc (date)
            if ($connection->fromEndpointInterface != NULL)
            {
                // rate is 5 minute. to get rate per second we need the 5 minutes and account for 60 seconds each minute
                $divider = 5 * 60;
                $rates   = $connection->fromEndpointInterface->rates->take(6);
                $count   = $rates->count() > 0 ? $rates->count() : 1;
                if (config("monitoring.diagram.overview.max"))
                {
                    $text .= DataRate::calc($rates->sum("in") / $divider / $count) . " (" .
                             DataRate::calc($rates->max("in") / $divider) . ")";
                    $text .= "\n" . DataRate::calc($rates->sum("out") / $divider / $count) . " (" .
                             DataRate::calc($rates->max("out") / $divider) . ")";
                } else
                {
                    $text .= DataRate::calc($rates->sum("in") / $divider /$count);
                    $text .= "\n" . DataRate::calc($rates->sum("out") / $divider / $count);
                }
            }
            if ($connection->toEndpointInterface != NULL && $connection->fromEndpointInterface == NULL)
            {
                // rate is 5 minute. to get rate per second we need the 5 minutes and account for 60 seconds each minute
                $divider = 5 * 60;
                $rates   = $connection->toEndpointInterface->rates->take(6);
                $count   = $rates->count() > 0 ? $rates->count() : 1;
                if (config("monitoring.diagram.overview.max"))
                {
                    $text .= DataRate::calc($rates->sum("in") / $divider / $count) . " (" .
                             DataRate::calc($rates->max("in") / $divider) . ")";
                    $text .= "\n" . DataRate::calc($rates->sum("out") / $divider / $count) . " (" .
                             DataRate::calc($rates->max("out") / $divider) . ")";
                } else
                {
                    $text .= DataRate::calc($rates->sum("in") / $divider / $count);
                    $text .= "\n" . DataRate::calc($rates->sum("out") / $divider / $count);
                }
            }
            if (!isset($filter[$connection->ip_to_id . "-" . $connection->ip_from_id]))
            {
                if (config("monitoring.diagram.overview.filter", TRUE))
                {
                    $filter[$connection->ip_to_id . "-" . $connection->ip_from_id] = 1;
                }
                $linkDataArray[] = ["from" => $connection->ip_to_id, "to" => $connection->ip_from_id, "text" => $text];
            }
        }
        
        return ["nodeDataArray" => array_values($nodeDataArray),
                "linkDataArray" => $linkDataArray,];
    }
}