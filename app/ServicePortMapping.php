<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ServicePortMapping extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $fillable   = ['protokoll', 'port', 'beschreibung', 'isMalware'];
    protected $casts      = ['isMalware' => 'boolean'];
    public    $timestamps = FALSE;
    
    public
    function connectionFrom()
    {
        return $this->hasMany('App\IPFix', ['id', 'protokoll'], ['from_port', 'protokoll']);
    }
    
    public
    function connectionTo()
    {
        return $this->hasMany('App\IPFix', ['id', 'protokoll'], ['to_port', 'protokoll']);
    }
    
    public static
    function reloadMapping()
    {
        ServicePortMapping::truncate();
        $in = explode(PHP_EOL, Storage::get(config("monitoring.service-mappings.import-file")));
        foreach ($in as $line)
        {
            $split   = explode(config("monitoring.service-mappings.delimiter"), str_replace('\r', '', $line));
            $malware = str_contains($line, ['worm', 'trojan', 'virus', 'rootkit']);
            if (count($split) > 1)
            {
                ServicePortMapping::firstOrCreate(['protokoll'    => $split[0],
                                                   'port'         => $split[1],
                                                   'beschreibung' => $split[2],
                                                   'isMalware'    => $malware]);
            }
        }
    }
}
