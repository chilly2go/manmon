<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class IP_Pool extends Model
{
    protected $fillable = ['ip',];
    
    public
    function connectionFrom()
    {
        return $this->hasMany("App\EndpointConnection", "ip_from_id", "id");
    }
    
    public
    function connectionTo()
    {
        return $this->hasMany("App\EndpointConnection", "ip_to_id", "id");
    }
    
    public
    function getConnectionsAttribute()
    {
        $connectionsFrom = $this->connectionFrom;
        $connectionsTo   = $this->connectionTo;
        
        return $connectionsFrom->merge($connectionsTo);
    }
    
    public
    function getLatencyAttribute($value)
    {
        return $value / 1000;
    }
    
    public
    function setLatencyAttribute($value)
    {
        $this->attributes['latency'] = $value * 1000;
    }
    
    public
    function getNameAttribute()
    {
        if ($this->attributes['name'] != "")
        {
            return $this->attributes['name'];
        }
        if (optional($this->endpoint)->name != NULL)
        {
            return $this->endpoint->name;
        }
        
        return $this->dataName;
    }
    
    public
    function getTreeNameAttribute()
    {
        $name = $this->name;
        $ping = $this->latency;
        if ($this->should_ping > 0)
        {
            return $name . "\n(Ping: " . $ping . "ms)";
        } else return $name;
    }
    
    public
    function getNameInfoAttribute()
    {
        $name = $this->name;
        
        return $name . ($name != $this->attributes['ip'] ? " (" . $this->attributes['ip'] . ")" : "");
    }
    
    public
    function getDataNameAttribute()
    {
        $name = $this->attributes['ip'];
        if (isset($this->data))
        {
            $data = json_decode($this->data);
            if (is_object($data))
            {
                $nameinfo = get_object_vars($data);
            } else
            {
                $nameinfo = array();
            }
            $parts = array();
            if (isset($nameinfo['as']))
            {
                $parts[] = $nameinfo['as'];
            } elseif (isset($nameinfo['combined']))
            {
                // dd($this->id, $nameinfo,"ippool->getDataNameAttribute()");
                $parts[] = $nameinfo['combined'];
            } else
            {
                $parts[] = $this->attributes['ip'];
            }
            if (isset($nameinfo['org']))
            {
                $parts[] = $nameinfo['org'];
            }
            if (isset($nameinfo['zip'], $nameinfo['city']))
            {
                $parts[] = $nameinfo['zip'] . " " . $nameinfo['city'];
            }
            if (isset($nameinfo['region']) && strlen($nameinfo['region']) > 0)
            {
                $parts[] = $nameinfo['region'];
            }
            if (isset($nameinfo['countryCode']))
            {
                $parts[] = $nameinfo['countryCode'];
            }
            $name = implode(", ", $parts);
        }
        
        return $name;
    }
    
    public
    function scopePing($query)
    {
        return $query->where("should_ping", '>', 0);
    }
    
    public
    function scopeNoData($query)
    {
        return $query->whereNull("data");
    }
    
    public
    function scopeOldest($query)
    {
        return $query->orderBy('updated_at', 'ASC');
    }
    
    public
    function scopeOlderThan($query, $hours)
    {
        return $query->where('updated_at', '<', Carbon::now()->subHours($hours));
    }
    
    public
    function scopeMissingLookup($query)
    {
        return $query->whereRaw("json_contains_path(data, 'one', '$.as') = 0")
                     ->WhereRaw("json_contains_path(data, 'one', '$.combined') = 0")
                     ->orWhereNull("data");
        
    }
    
    public
    function scopePublicAddress($query)
    {
        return $query->where(function ($query)
        {
            $query->where('ip', '!=', '0.0.0.0')
                  ->where('ip', 'not like', '10.%')
                  ->where('ip', 'not like', '172.16.%')
                  ->where('ip', 'not like', '172.17.%')
                  ->where('ip', 'not like', '172.18.%')
                  ->where('ip', 'not like', '172.19.%')
                  ->where('ip', 'not like', '172.20.%')
                  ->where('ip', 'not like', '172.21.%')
                  ->where('ip', 'not like', '172.22.%')
                  ->where('ip', 'not like', '172.23.%')
                  ->where('ip', 'not like', '172.24.%')
                  ->where('ip', 'not like', '172.25.%')
                  ->where('ip', 'not like', '172.26.%')
                  ->where('ip', 'not like', '172.27.%')
                  ->where('ip', 'not like', '172.28.%')
                  ->where('ip', 'not like', '172.29.%')
                  ->where('ip', 'not like', '172.30.%')
                  ->where('ip', 'not like', '172.31.%')
                  ->where('ip', 'not like', '192.168.%')
                  ->where('ip', 'not like', '224.%')
                  ->where('ip', 'not like', '225.%')
                  ->where('ip', 'not like', '226.%')
                  ->where('ip', 'not like', '227.%')
                  ->where('ip', 'not like', '228.%')
                  ->where('ip', 'not like', '229.%')
                  ->where('ip', 'not like', '230.%')
                  ->where('ip', 'not like', '231.%')
                  ->where('ip', 'not like', '232.%')
                  ->where('ip', 'not like', '233.%')
                  ->where('ip', 'not like', '234.%')
                  ->where('ip', 'not like', '235.%')
                  ->where('ip', 'not like', '236.%')
                  ->where('ip', 'not like', '237.%')
                  ->where('ip', 'not like', '238.%')
                  ->where('ip', 'not like', '239.%')
                  ->where('ip', 'not like', '240.%')
                  ->where('ip', 'not like', '241.%')
                  ->where('ip', 'not like', '242.%')
                  ->where('ip', 'not like', '243.%')
                  ->where('ip', 'not like', '244.%')
                  ->where('ip', 'not like', '245.%')
                  ->where('ip', 'not like', '246.%')
                  ->where('ip', 'not like', '247.%')
                  ->where('ip', 'not like', '248.%')
                  ->where('ip', 'not like', '249.%')
                  ->where('ip', 'not like', '250.%')
                  ->where('ip', 'not like', '251.%')
                  ->where('ip', 'not like', '252.%')
                  ->where('ip', 'not like', '253.%')
                  ->where('ip', 'not like', '254.%')
                  ->where('ip', '!=', '255.255.255.255');
        });
        
    }
    
    public
    function scopePrivateAddress($query)
    {
        return $query->where(function ($query)
        {
            $query->where('ip', 'like', '10.%')
                  ->orWhere('ip', 'like', '172.16.%')
                  ->orWhere('ip', 'like', '172.17.%')
                  ->orWhere('ip', 'like', '172.18.%')
                  ->orWhere('ip', 'like', '172.19.%')
                  ->orWhere('ip', 'like', '172.20.%')
                  ->orWhere('ip', 'like', '172.21.%')
                  ->orWhere('ip', 'like', '172.22.%')
                  ->orWhere('ip', 'like', '172.23.%')
                  ->orWhere('ip', 'like', '172.24.%')
                  ->orWhere('ip', 'like', '172.25.%')
                  ->orWhere('ip', 'like', '172.26.%')
                  ->orWhere('ip', 'like', '172.27.%')
                  ->orWhere('ip', 'like', '172.28.%')
                  ->orWhere('ip', 'like', '172.29.%')
                  ->orWhere('ip', 'like', '172.30.%')
                  ->orWhere('ip', 'like', '172.31.%')
                  ->orWhere('ip', 'like', '192.168.%');
        });
    }
    
    public
    function endpoint()
    {
        return $this->hasOne("App\SNMP_Endpoint", "ip_id", "id");
    }
}
