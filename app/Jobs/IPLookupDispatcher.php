<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 */

namespace App\Jobs;

use App\Helpers\IP_Class_Examiner;
use App\IP_Pool;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IPLookupDispatcher implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public  $timeout = 90;
    private $uuid;
    private $start;
    
    public
    function __construct()
    {
        $this->uuid  = str_random(12);
        $this->start = Carbon::now()->toDateTimeString();
    }
    
    private
    function checkLookup(string $ip_string, array &$private, array &$public)
    {
        if ($this->lookup_ip_last_mod_enabled)
        {
            $ip = IP_Pool::firstOrCreate(['ip' => $ip_string]);
        } else
        {
            $ip = $ip_string;
        }
        // if enabled -> check last mod time
        if (($this->lookup_ip_last_mod_enabled &&
             $ip->updated_at->lt(Carbon::now()->subHours($this->lookup_ip_last_mod_hours))))
        {
            switch (IP_Class_Examiner::getIPv4Class($ip))
            {
                case IP_Class_Examiner::PRIVATE:
                    $private[$ip->ip] = $ip;
                    break;
                case IP_Class_Examiner::PUBLIC:
                    $public[$ip->ip] = $ip;
                    break;
                case IP_Class_Examiner::MULTICAST:
                    $ip->ip->data = json_encode(['class' => 'Multicast']);
                    $ip->save();
            }
        } else
        {
            switch (IP_Class_Examiner::getIPv4Class($ip))
            {
                case IP_Class_Examiner::PRIVATE:
                    $private[$ip] = $ip;
                    break;
                case IP_Class_Examiner::PUBLIC:
                    $public[$ip] = $ip;
                    break;
            }
        }
    }
    
    public
    function handle()
    {
        // \Log::info("[IPLookupDispatcher](" . $this->start . "): Getting private ips");
        $private = IP_Pool::privateAddress()->missingLookup()->oldest()->take(10)->get();
        if ($private != NULL)
        {
            \Log::info("[IPLookupDispatcher](" . $this->start . "): Dispatching private ip lookup");
            $private->each(function ($item, $key)
            {
                IPLookupHostname::dispatch($item)->onQueue("lookup")->delay($key * 1);
            });
        }
        // \Log::info("[IPLookupDispatcher](" . $this->start . "): Getting \"old\" private ips");
        $private = IP_Pool::privateAddress()->olderThan(24)->take(5)->get();
        if ($private != NULL && $private->count() > 0)
        {
            \Log::info("[IPLookupDispatcher](" . $this->start . "): Dispatching private ips to update old values");
            $i = 0;
            $private->each(function ($item, $key) use (&$i)
            {
                IPLookupHostname::dispatch($item)->onQueue("lookup")->delay($key * 1);
            });
        }
        \Log::info("[IPLookupDispatcher](" . $this->start . "): Getting public ips");
        $public =
            IP_Pool::publicAddress()->missingLookup()->oldest()->take(config("monitoring.csv.lookup.check.chunk-size",
                                                                             70))->get();
        if ($public != NULL)
        {
            $ips = $public->chunk(10);
            \Log::info("[IPLookupDispatcher](" . $this->start . "): Chunking public ips (total:" . $public->count() .
                       "/chunks:" . count($ips) . ")");
            $i = 0;
            if (count($ips) > 0)
            {
                foreach ($ips as $ip_chunk)
                {
                    // \Log::info("[IPLookupDispatcher](" . $this->start . "): Dispatching chunk (" . $i++ . "/" .
                    //            count($ips) . ")");
                    IPLookupGeolocation::dispatch($ip_chunk)->onQueue("lookup")->delay((5 * $i));
                    // sleep(5);
                }
            }
        }
        \Log::info("[IPLookupDispatcher](" . $this->start . "): Getting old public ips");
        $public = IP_Pool::publicAddress()->oldest()->take(config("monitoring.csv.lookup.check.chunk-size", 70))->get();
        if ($public != NULL)
        {
            $ips = $public->chunk(10);
            \Log::info("[IPLookupDispatcher](" . $this->start . "): Chunking old public ips (total:" .
                       $public->count() . "/chunks:" . count($ips) . ")");
            $i = 0;
            if (count($ips) > 0)
            {
                foreach ($ips as $ip_chunk)
                {
                    // \Log::info("[IPLookupDispatcher](" . $this->start . "): (old)Dispatching chunk (" . $i++ . "/" .
                    //            count($ips) . ")");
                    IPLookupGeolocation::dispatch($ip_chunk)->onQueue("lookup")->delay((5 * $i));
                    // sleep(5);
                }
            }
        }
    }
    
    public
    function tags()
    {
        return ["IP_Lookup_Dispatch", $this->start];
    }
}