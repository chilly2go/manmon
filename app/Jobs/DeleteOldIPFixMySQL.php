<?php

namespace App\Jobs;

use App\Helpers\Ping;
use App\Helpers\SNMP\OID;
use App\Helpers\SNMP\SNMP;
use App\Helpers\SNMP\SNMP_Adress;
use App\IP_Pool;
use App\IPFix;
use App\SNMP_Endpoint;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteOldIPFixMySQL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * @var SNMP_Endpoint
     */
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct()
    {
        //
        $this->start = Carbon::now();
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        $deleted = IPFix::old()->delete();
        if (config("monitoring.csv.line-chunks.mysql.log_affected_rows", FALSE))
        {
            \Log::info("[DeleteOldIPFixMySQL]: Deleted IPFix Rows: " . $deleted);
        }
    }
    
    public
    function tags()
    {
        return ["IPFixDeleteOld"];
    }
}
