<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class ConvertFileCheckerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $start;
    public  $timeout = 120;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct()
    {
        $this->start = Carbon::now()->toDateTimeString();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        // get endpoint directories
        $i     = 0;
        $delay = config("monitoring.csv.check-files.delay");
        foreach (Storage::directories("netflow") as $dir)
        {
            // get directories containing nfcapd files
            foreach (Storage::directories($dir) as $subdir)
            {
                collect(Storage::files($subdir))->take(2)->each(function ($item, $key) use ($dir, &$i, $delay)
                {
                    ConvertNfcapdFilesToCSV::dispatch($item, pathinfo($dir)['basename'])->onQueue("csv")->delay($i++ *
                                                                                                                $delay);
                });
            }
        }
    }

    public
    function tags()
    {
        return ["Convert_Check_Files", $this->start];
    }
}
