<?php

namespace App\Jobs;

use App\Helpers\Ping;
use App\Helpers\SNMP\OID;
use App\Helpers\SNMP\SNMP;
use App\Helpers\SNMP\SNMP_Adress;
use App\IP_Pool;
use App\SNMP_Endpoint;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PingIP implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var SNMP_Endpoint
     */
    private $ip;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct(IP_Pool $ip)
    {
        //
        $this->ip = $ip;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        $ttl               = 20;
        $timeout           = 5;
        $this->ip->latency = (new Ping($this->ip->ip, $ttl, $timeout))->ping();
        $this->ip->save();
    }

    public
    function tags()
    {
        return ["PING_IP", optional($this->ip->endpoint)->name ?: $this->ip->name, "Delay: " . $this->delay];
    }
}
