<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class ConvertNfcapdFilesToCSV implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $file;
    private $dirfilter;
    public  $timeout = 240;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct($file, $dirfilter)
    {
        //
        $this->file      = $file;
        $this->dirfilter = $dirfilter;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        /**
         * 1. get full path
         * 2. convert binary to csv and store in var
         * 3. extract lines
         * 4. chunk lines
         * 5. dispatch chunks to be handled
         * 6. delete file
         */
        // \Log::info("[ConvertNfcapdFilesToCSV]" . $this->file . ': Reading file');
        $fullpath = storage_path("app/" . $this->file);
        $command  = "nfdump -r " . $fullpath . " -B -q -o csv";
        $output   = shell_exec($command);
        $lines    = explode(PHP_EOL, $output); // split into lines
        $data     = array();
        // handle lines
        foreach ($lines as $line)
        {
            // as we currently don't use more than column 13 we can drop off some data and free memore
            array_push($data,
                       array_slice(explode(",", $line),
                                   config("monitoring.csv.check-files.cols.from"),
                                   config("monitoring.csv.check-files.cols.to")));
        }
        $d_count     = count($data);
        $type        = config("monitoring.csv.line-chunks.type");
        $data        = array_chunk($data, config("monitoring.csv.line-chunks." . $type . ".size", 10));
        $i           = 0;
        $chunk_delay = config("monitoring.csv.line-chunks." . $type . ".delay");
        // dispatch chunks
        foreach ($data as $chunk)
        {
            if ($type == "influx")
            {
                NfdumpCsvHanderInflux::dispatch($chunk, $this->dirfilter, "c" . $i++)->onQueue("csv")->delay($i++ *
                                                                                                             $chunk_delay);
            } elseif ($type == "mysql")
            {
                NfdumpCsvHanderMySQL::dispatch($chunk, $this->dirfilter, "c" . $i++)->onQueue("csv")->delay($i++ *
                                                                                                            $chunk_delay);
            }
        }
        Storage::delete($this->file);
    }
    
    public
    function tags()
    {
        return ["CONVERT_NFCAPD", $this->file];
    }
}
