<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 */

namespace App\Jobs;

use App\IP_Pool;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IPLookupGeolocation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $ip;
    public  $timeout = 10;
    private $ip_count;
    private $tries   = array();
    
    public
    function __construct($ip)
    {
        $this->ip       = $ip;
        $this->ip_count = $ip->count();
        array_push($this->tries, "Create: " . Carbon::now()->toDateTimeString());
    }
    
    public
    function handle()
    {
        if (config("monitoring.lookup.check.enabled", TRUE))
        {
            switch (config("monitoring.lookup.endpoint", "IPAPI"))
            {
                case "IPAPI":
                    IPLookupGeolocationIPAPI::dispatch($this->ip)->onQueue("lookup");
                    break;
                case "IPDATA":
                    $i = 0;
                    foreach ($this->ip as $ip)
                    {
                        IPLookupGeolocationIPData::dispatch($ip)->onQueue("lookup")->delay($i++);
                    }
                    break;
                default:
                    \Log::error("Invalid Lookup endpoint for external IP Addresses: " .
                                config("monitoring.lookup.endpoint", "IPAPI"));
            }
        }
    }
    
    public
    function tags()
    {
        return ["IP_Geolocation_Lookup", $this->delay];
    }
}