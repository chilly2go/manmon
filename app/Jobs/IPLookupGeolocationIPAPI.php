<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 * Endpoint: IP-API.com
 */

namespace App\Jobs;

use App\IP_Pool;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IPLookupGeolocationIPAPI implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $ip;
    public  $timeout = 120;
    private $ip_count;
    private $tries   = array();
    
    public
    function __construct($ip)
    {
        $this->ip       = $ip;
        $this->ip_count = $ip->count();
        array_push($this->tries, "Create: " . Carbon::now()->toDateTimeString());
    }
    
    public
    function handle()
    {
        try
        {
            // \Log::info("[IPLookupGeolocation](" . $this->ip_count . "): Starting Guzzle Client");
            $client = new Client(
                
                [// Base URI is used with relative requests
                 'base_uri' => 'http://ip-api.com',
                 // You can set any number of default request options.
                 'timeout'  => 3.0,]);
            $query  = array();
            if ($this->ip instanceof Collection)
            {
                $query = $this->ip->pluck('ip')->map(function ($item, $key) { return ['query' => $item]; })->toArray();
            }
            // batch handling for 1 ip? nope
            if (count($query) == 1)
            {
                $this->ip = array_pop($query);
            }
            // more than 1 ip to query?
            if (count($query) > 1)
            {
                $response = $client->request('POST', 'batch', ['json' => $query]);
            } else
            {
                $response = $client->request('GET', 'json/' . $this->ip->ip);
            }
            if ($response->getStatusCode() == 200)
            {
                if (count($query) > 1)
                {
                    $data = json_decode($response->getBody()->getContents());
                    $ips  = $this->ip->reverse();
                    for ($i = 0; $i < $this->ip->count(); $i++)
                    {
                        $ip = $ips->pop();
                        if ($ip->ip == $data[$i]->query)
                        {
                            $ip->data = json_encode($data[$i]);
                            $ip->save();
                        } else
                        {
                            \Log::info("[IPLookupGeolocation](" . $this->ip_count . "): Error Updating: ip:" .
                                       optional($ip)->ip . "|query:" . $data[$i]->query . "|status:" .
                                       $data[$i]->status);
                        }
                    }
                } else
                {
                    if (!$this->ip instanceof IP_Pool)
                    {
                        IP_Pool::firstOrFail(['ip' => $this->ip]);
                    }
                    $this->ip->data = json_encode($response->getBody()->getContents());
                    $this->ip->save();
                }
            }
        } catch (\Exception $e)
        {
            dd($e, $this->ip, 2, is_array($this->ip), $query);
        }
    }
    
    public
    function tags()
    {
        return ["IP_Geolocation_Lookup", $this->delay];
    }
}