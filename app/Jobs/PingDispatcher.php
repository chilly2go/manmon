<?php

namespace App\Jobs;

use App\IP_Pool;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PingDispatcher implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $start;
    public  $timeout = 120;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct()
    {
        $this->start = Carbon::now()->format("H:i");
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        $delay = config("monitoring.ping.dispatch.delay");
        IP_Pool::ping()->each(function ($item, $key) use ($delay)
        {
            PingIP::dispatch($item)->onQueue("ping")->delay(($key * $delay));
        });
    }

    public
    function tags()
    {
        return ["PingDispatch", $this->start];
    }
}
