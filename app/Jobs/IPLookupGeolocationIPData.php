<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 */

namespace App\Jobs;

use App\IP_Pool;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class IPLookupGeolocationIPData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $ip;
    public  $timeout = 20;
    private $ip_count;
    private $tries   = array();
    
    public
    function __construct(IP_Pool $ip)
    {
        $this->ip = $ip;
        array_push($this->tries, "Create: " . Carbon::now()->toDateTimeString());
    }
    
    public
    function handleJob()
    {
        try
        {
            // \Log::info("[IPLookupGeolocation](" . $this->ip_count . "): Starting Guzzle Client");
            $client = new Client(
                
                [// Base URI is used with relative requests
                 'base_uri' => 'https://api.ipdata.co/',
                 // You can set any number of default request options.
                 'timeout'  => 10.0,]);
            
            $response = $client->request('GET',
                                         $this->ip->ip . '/de?api-key=' .
                                         config("monitoring.lookup.endpoints.IPDATA.API_KEY", "none"));
            
            if ($response->getStatusCode() == 200)
            {
                $data           = json_decode($response->getBody()->getContents());
                $data->org      = $data->organisation;
                $this->ip->data = json_encode($data);
                $this->ip->save();
            }
        } catch (\Exception $e)
        {
            \Log::warning("[IPLookupGeolocationIPData]: " . $e->getMessage());
            // dd($e, $this->ip, 2, is_array($this->ip), $query);
        }
    }
    
    public
    function handle()
    {
        if (config("monitoring.lookup.check.enabled", TRUE))
        {
            Redis::throttle('external')->allow(250)->every(14400)->then(function ()
            {
                $this->handleJob();
            },
                function ()
                {
                    // low rate limit. if we exceed it we just fail
                    return $this->delete();
                });
        }
    }
    
    public
    function tags()
    {
        return ["IP_Geolocation_Lookup", "IP_DATA", $this->delay, $this->ip->ip];
    }
}