<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 */

namespace App\Jobs;

use App\Helpers\IPFix\LineHandler;
use App\IP_Pool;
use App\IPFix;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class NfdumpCsvHanderMySQL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $data;
    private $dir;
    private $data_count;
    private $created = array();
    public  $timeout = 120;
    private $log;
    private $uuid;
    private $chunk_id;
    
    public
    function __construct($data, $dir, $chunk_id)
    {
        $this->dir        = $dir;
        $this->data       = $data;
        $this->data_count = count($this->data);
        $this->created    = "Create: " . Carbon::now()->toDateTimeString();
        $this->log        = array();
        $this->uuid       = str_random(12);
        $this->chunk_id   = $chunk_id;
    }
    
    /**
     * parse each line (skip if first col contains "summary")
     * create influxdb point for each line
     * check if lookup for IP is in order
     */
    public
    function handle()
    {
        $points = array();
        // foreach line create an influx datapoint
        try
        {
            $start = NULL;
            $end   = NULL;
            $ips   = array();
            foreach ($this->data as $line)
            {
                $tmp = LineHandler::handleIPsOnly($line);
                switch ($tmp)
                {
                    case 0:
                    case -1:
                        break;
                    default:
                        $ips[$tmp['from_ip']] = $tmp['from_ip'];
                        $ips[$tmp['to_ip']]   = $tmp['to_ip'];
                }
            }
            foreach ($ips as $ip)
            {
                $tmp           = IP_Pool::firstOrCreate(['ip' => $ip]);
                $ips[$tmp->ip] = $tmp->id;
            }
            foreach ($this->data as $line)
            {
                $tmp = LineHandler::handle($line);
                switch ($tmp)
                {
                    case 0:
                    case -1:
                        break;
                    default:
                        $tmp_ipfix = IPFix::create(['from_ip'     => $ips[$tmp['from_ip']],
                                                    'from_port'   => $tmp['from_port'],
                                                    'to_ip'       => $ips[$tmp['to_ip']],
                                                    'to_port'     => $tmp['to_port'],
                                                    'start'       => $tmp['start'],
                                                    'end'         => $tmp['end'],
                                                    'duration'    => $tmp['duration'],
                                                    'bytes_in'    => $tmp['packet_bytes_in'],
                                                    'packets_in'  => $tmp['packet_count_in'],
                                                    'bytes_out'   => $tmp['packet_bytes_out'],
                                                    'packets_out' => $tmp['packet_count_out'],
                                                    'protokoll'   => $tmp['protocol'],
                                                    'flags'       => $tmp['flags'],]);
                        if (config("monitoring.csv.line-chunks.mysql.calc_features", FALSE))
                        {
                            IPFixFeatureCalc::dispatch($tmp_ipfix->id)->onQueue("csv");
                        }
                }
            }
        } catch (FatalThrowableError $throwableError)
        {
            \Log::info("[NfdumpCsvHanderMySQL]: from: " . $line[0] . "| to: " . $line[1] . "| message" .
                       $throwableError->getMessage());
            $this->fail($throwableError);
        } catch (\Exception $e)
        {
            \Log::info("[NfdumpCsvHanderMySQL]: from: " . $line[0] . "| to: " . $line[1] . "| message" .
                       $e->getMessage() . " | " . json_encode(array_pop($this->data)));
            $this->fail($e);
        }
    }
    
    public
    function tags()
    {
        return ["Handle_CSV_Output", $this->data_count, $this->dir, $this->chunk_id];
    }
    
}