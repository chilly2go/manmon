<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 */

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Helpers\InfluxDataInterface;
use Carbon\Carbon;
use InfluxDB\Exception;
use InfluxDB\Point;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class NfdumpCsvHanderInflux implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $data;
    private $dir;
    private $data_count;
    private $created = array();
    public  $timeout = 120;
    private $log;
    private $uuid;
    private $chunk_id;

    public
    function __construct($data, $dir, $chunk_id)
    {
        $this->dir        = $dir;
        $this->data       = $data;
        $this->data_count = count($this->data);
        $this->created    = "Create: " . Carbon::now()->toDateTimeString();
        $this->log        = array();
        $this->uuid       = str_random(12);
        $this->chunk_id   = $chunk_id;
    }

    /**
     * parse each line (skip if first col contains "summary")
     * create influxdb point for each line
     * check if lookup for IP is in order
     */
    public
    function handle()
    {
        $points = array();
        // foreach line create an influx datapoint
        try
        {
            $start = NULL;
            $end   = NULL;
            $ips   = array();
            foreach ($this->data as $line)
            {
                if (count($line) < 12)
                {
                    continue;
                }
                if ($line[0] == "Summary")
                {
                    break;
                }
                $start        = Carbon::parse($line[0]);
                $end          = Carbon::parse($line[1]);
                $duration     = $line[2];
                $from_ip      = $line[3];
                $from_port    = $line[5];
                $to_ip        = $line[4];
                $to_port      = $line[6];
                $protocol     = $line[7];
                $flags        = str_replace([".", "A", "S", "F", "R", "P", "U", "X"],
                                            ["", "ACK,", "SYN,", "FIN,", "Reset,", "Push,", "Urgent,", "All Flags,"],
                                            $line[8]);
                $packet_count = $line[11];
                $packet_bytes = $line[12];
                $points[]     = new Point("ipfix", $duration, ["protokoll" => $protocol,
                                                               "from_ip"   => $from_ip,
                                                               "from_port" => $from_port,
                                                               "to_ip"     => $to_ip,
                                                               "to_port"   => $to_port,
                                                               // "flags"     => $flags,
                                                               "start"     => $start->timestamp,
                                                               // "end"       => $end->timestamp
                ],
                                          ["packets" => $packet_count, "bytes" => $packet_bytes], $start->timestamp);
                // \Log::info("[IPFix Line]:" . json_encode($line));
                $ips[$from_ip] = $from_ip;
                $ips[$to_ip]   = $to_ip;
            }
            // \Log::info("[NfdumpCsvHanderInflux]: dir: " . $this->dir . "| chunk: " . $this->chunk_id . "| start: " . $start .
            //            "(" . $line[0] . ") | end: " . $end);
            if (count($points) > 0)
            {
                // \Log::info("[NfdumpCsvHanderInflux]: dir: " . $this->dir . "| chunk: " . $this->chunk_id .
                //            "| writing to influx. points: " . count($points));
                $influx = new InfluxDataInterface();
                $influx->write($points);
            } else
            {
                \Log::info("[NfdumpCsvHanderInflux]: dir: " . $this->dir . "| chunk: " . $this->chunk_id .
                           "| no points. count data: " . count($this->data) . " | line cols: " . count($this->data[0]));
            }
        } catch (Exception $influx_e)
        {
            \Log::warning("[NfdumpCsvHanderInflux]: dir: " . $this->dir . "| chunk: " . $this->chunk_id . " |" .
                          $influx_e->getMessage() . " | total points: " . count($points));
            $this->delete();
        } catch (FatalThrowableError $throwableError)
        {
            \Log::info("[NfdumpCsvHanderInflux]: from: " . $line[0] . "| to: " . $line[1] . "| message" .
                       $throwableError->getMessage());
            $this->fail($throwableError);
        } catch (\Exception $e)
        {
            \Log::info("[NfdumpCsvHanderInflux]: from: " . $line[0] . "| to: " . $line[1] . "| message" . $e->getMessage() .
                       " | " . json_encode(array_pop($this->data)));
            $this->fail($e);
        }
    }

    public
    function tags()
    {
        return ["Handle_CSV_Output", $this->data_count, $this->dir, $this->chunk_id];
    }

}