<?php
/**
 * Created by IntelliJ IDEA.
 * User: cookie
 * Date: 28.08.2018
 * Time: 19:41
 */

namespace App\Jobs;

use App\IP_Pool;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IPLookupHostname implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $ip;
    public  $timeout = 30;
    private $tries   = array();
    
    public
    function __construct($ip)
    {
        $this->ip = $ip;
        array_push($this->tries, "Create: " . Carbon::now()->toDateTimeString());
    }
    
    public
    function handle()
    {
        if (config("monitoring.lookup.check.enabled", TRUE))
        {
            try
            {
                if (!($this->ip instanceof IP_Pool))
                {
                    $ip = IP_Pool::where('ip', $this->ip)->first();
                } else
                {
                    $ip = $this->ip;
                }
                if ($ip == NULL)
                {
                    throw new \Exception("Can't find IP-instance of " . $this->ip);
                }
                $command = "timeout 3 nmblookup -A " . $ip->ip;
                $output  = shell_exec($command);
                $data    = explode("\n", $output);
                $return  = array();
                if (count($data) > 1)
                {
                    foreach ($data as $line)
                    {
                        $line = str_replace("\t", "", $line);
                        if (str_contains($line, "<00>"))
                        {
                            array_push($return, explode(" ", $line)[0]);
                        }
                    }
                }
                if (count($return) >= 1)
                {
                    $data = array();
                    if (isset($return[0]))
                    {
                        $data['hostname'] = $return[0];
                    }
                    if (isset($return[1]))
                    {
                        $data['groupname'] = $return[1];
                    }
                    $data['combined'] = implode(".", $return);
                    $ip->data         = json_encode($data);
                } else
                {
                    $dataObj = json_decode(empty($ip->data) ? "{}" : $ip->data);
                    if (isset($ip->data, get_object_vars(json_decode($ip->data))['hostname']))
                    {
                        $dataObj->recent = "Update Überprüfung fehlgeschlagen. Keine Antwort";
                    } else
                    {
                        $dataObj->could_not_match = !empty($output) ? $output : $command;
                        // \Log::info("[IPLookupHostname]: Output: " . $output . " | Command: " . $command);
                    }
                    $ip->data = json_encode($dataObj);
                    // \Log::info("[IPLookupHostname]: Data: " . $ip->data);
                }
                $ip->save();
                
            } catch (\Exception $e)
            {
                \Log::info("[IPLookupHostname]: IP " . $this->ip . " - Error:" . $e->getMessage() . " - Data: " .
                           $ip->data);
                throw new \Exception($e);
            }
        }
    }
    
    public
    function tags()
    {
        return ["IP_Hostname_Lookup", $this->ip instanceof IP_Pool ? $this->ip->ip : $this->ip, $this->delay];
    }
}