<?php

namespace App\Jobs;

use App\Helpers\InfluxDataInterface;
use App\Helpers\SNMP\OID;
use App\Helpers\SNMP\SNMP;
use App\Helpers\SNMP\SNMP_DataChangeTracker;
use App\SNMP_Endpoint;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use InfluxDB\Exception;

class SNMPPollData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var SNMP_Endpoint
     */
    private $endpoint;
    private $points;
    const metrics = ["inoctets", "indiscards", "inerrors", "outoctets", "outdiscards", "outerrors",];
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct(SNMP_Endpoint $endpoint)
    {
        //
        $this->endpoint = $endpoint;
        $this->points   = array();
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        $ep = $this->endpoint;
        $ep->load("interfaces");
        $snmp       = new SNMP($ep->ip->ip, $ep->community);
        $interfaces = $snmp->walk(OID::getData());
        $mod        = $ep->interfaces->filter(function ($item, $key) use ($interfaces)
        {
            if (isset($interfaces['portstatus'][$item->index]))
            {
                $values = array();
                foreach ($interfaces as $index => $val)
                {
                    if (in_array($index, SNMPPollData::metrics))
                    {
                        $values[$index][$item->index] = $interfaces[$index][$item->index];
                    }
                }
                // get changes
                $old_values = json_decode($item->values);
                $point      =
                    (new SNMP_DataChangeTracker($item, $old_values, $values, $this->endpoint->id))->handleChanges();
                // send data to influx
                if ($point !== NULL)
                {
                    $this->points[] = $point;
                }
                // store current state
                $item->values = json_encode($values);
                
                // $item->save();
                return $item;
            }
        });
        if (config("monitoring.snmp.poll.datastore", "mysql") == "influx")
        {
            try
            {
                $influx = new InfluxDataInterface();
                $influx->write($this->points);
            } catch (Exception $e)
            {
                \Log::info("[SNMPPollData]: Influx Error: " . $e->getMessage());
                $this->delete();
            }
        }
        $mod->each(function ($item, $key)
        {
            $item->save();
        });
    }
    
    public
    function tags()
    {
        return ["SNMP_Poll", $this->endpoint->name];
    }
}
