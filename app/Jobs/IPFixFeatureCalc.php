<?php

namespace App\Jobs;

use App\Helpers\Ping;
use App\Helpers\SNMP\OID;
use App\Helpers\SNMP\SNMP;
use App\Helpers\SNMP\SNMP_Adress;
use App\IP_Pool;
use App\IPFix;
use App\SNMP_Endpoint;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use PDOException;

class IPFixFeatureCalc implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * @var SNMP_Endpoint
     */
    private $ipfix_id;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct(int $ipfix_id)
    {
        $this->ipfix_id = $ipfix_id;
        $this->start    = Carbon::now();
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        try
        {
            DB::select('call sp_IPFix_Calculate_Features(' . $this->ipfix_id . ')');
        } catch (PDOException|\Doctrine\DBAL\Driver\PDOException $e)
        {
            \Log::warning("[IPFixFeatureCalc](" . $this->attempts() .
                          ") Error trying to execute stored procedure");
            $this->delete(); // only 1 retry!
        }
    }
    
    public
    function tags()
    {
        return ["IPFixFeatureCalc"];
    }
}
