<?php

namespace App\Jobs;

use App\Helpers\SNMP\OID;
use App\Helpers\SNMP\SNMP;
use App\Helpers\SNMP\SNMP_Adress;
use App\SNMP_Endpoint;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SNMPGatherInitialData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var SNMP_Endpoint
     */
    private $endpoint;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public
    function __construct(SNMP_Endpoint $endpoint)
    {
        //
        $this->endpoint = $endpoint;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public
    function handle()
    {
        $snmp                         = new SNMP($this->endpoint->ip->ip, $this->endpoint->community);
        $info                         = $snmp->walk(OID::getEndpointInfo());
        $this->endpoint->beschreibung = $info['descr'][0];
        $this->endpoint->uptime       = $info['uptime'][0];
        $this->endpoint->kontakt      = $info['contact'][0];
        $this->endpoint->name         = $info['name'][0];
        $this->endpoint->ort          = $info['location'][0];
        $this->endpoint->save();
        $interfaces = $snmp->walk(OID::getInterfaceInfo());
        $addresses  = (new SNMP_Adress($interfaces['address']))->getAdressesMAC();
        $i          = 0;
        foreach ($interfaces["index"] as $key => $val)
        {
            // only the minimum number of fields required to identify the interface. everything else comes later (to avoid duplicates
            $if = $this->endpoint->interfaces()->firstOrNew(

                ["endpoint_id" => $this->endpoint->id,
                 "name"        => trim($interfaces['description'][$key]),]

            );
            // set other values / fields
            $if->speed = $interfaces['speed'][$key];
            $if->mac   = $addresses[$i++];
            $if->mtu   = $interfaces['mtu'][$key];
            $if->index = $key;
            // save
            $if->save();
        }
    }

    public
    function tags()
    {
        return ["SNMP_Endpoint", $this->endpoint->name];
    }
}
