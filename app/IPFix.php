<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class IPFix extends Model
{
    use \Awobaz\Compoships\Compoships;
    
    protected $table    = "ipfix";
    protected $fillable = ['from_ip',
                           'from_port',
                           'to_ip',
                           'to_port',
                           'start',
                           'end',
                           'duration',
                           'bytes_in',
                           'packets_in',
                           'bytes_out',
                           'packets_out',
                           'protokoll',
                           'flags',
                           '26',
                           '27',
                           '30',
                           '31',
                           '34',
                           '35',
                           '38',];
    protected $casts    = ['isFeatureCalucalted' => 'boolean'];
    protected $dates    = ['start', 'end', 'created_at', 'updated_at'];
    
    public
    function from()
    {
        return $this->belongsTo('App\IP_Pool', 'from_ip');
    }
    
    public
    function to()
    {
        return $this->belongsTo('App\IP_Pool', 'to_ip');
    }
    
    public
    function fromPort()
    {
        return $this->belongsTo('App\ServicePortMapping', ['from_port', 'protokoll'], ['id', 'protokoll']);
    }
    
    public
    function toPort()
    {
        return $this->belongsTo('App\ServicePortMapping', ['to_port', 'protokoll'], ['id', 'protokoll']);
    }
    
    public
    function getFlagsAttribute($value)
    {
        return str_replace([".", "A", "S", "F", "R", "P", "U", "X"],
                           ["", "ACK,", "SYN,", "FIN,", "Reset,", "Push,", "Urgent,", "All Flags,"],
                           $value);
    }
    
    public
    function scopeCurrent($query)
    {
        
        return $query->where('start', '>', Carbon::now()->subHour()->timestamp);
    }
    
    public
    function scopeToCalculate($query)
    {
        return $query->where('isFeatureCalucalted', 0);
    }
    
    public
    function scopeOld($query)
    {
        return $query->where('end', '<', Carbon::now()->subHours(2));
    }
}
