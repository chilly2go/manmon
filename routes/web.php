<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ShowHome')->name("home");
Route::get("/tester", "ShowHome");
Route::prefix('ip')->group(function ()
{
    Route::get("/", "IPController@index")->name("ip.index");
    // Route::get("new", "IPController@index")->name("ip.new");
    Route::post("store", "IPController@store")->name("ip.store");
    Route::get("show/{ip}", "IPController@show")->name("ip.show");
    Route::get("edit/{ip}", "IPController@edit")->name("ip.edit");
    Route::post("update/{ip}", "IPController@update")->name("ip.update");
    Route::get("del/{ip}", "IPController@destroy")->name("ip.destroy");
});
Route::prefix('endpoints')->group(function ()
{
    Route::get("/", "EndpointController@index")->name("endpoint.index");
    Route::post("store", "EndpointController@store")->name("endpoint.store");
    Route::get("show/{endpoint}", "EndpointController@show")->name("endpoint.show");
    Route::get("edit/{endpoint}", "EndpointController@edit")->name("endpoint.edit");
    Route::post("update/{endpoint}", "EndpointController@update")->name("endpoint.update");
    Route::get("del/{endpoint}", "EndpointController@destroy")->name("endpoint.destroy");
});
Route::prefix('connection')->group(function ()
{
    Route::get("/", "ConnectionsController@index")->name("connection.index");
    Route::get("/new", "ConnectionsController@create_step1")->name("connection.create.step1");
    Route::post("/new/endpoint", "ConnectionsController@create_step2")->name("connection.create.step2");
    Route::post("/new/endpoint/interface", "ConnectionsController@create_step3")->name("connection.create.step3");
    Route::get("/delete/{connection}", "ConnectionsController@delete")->name("connection.delete");
});
Route::prefix('diagram')->group(function ()
{
    Route::get("/sankey/ipfix", "DiagramController@sankeyIPFix")->name("diagram.sankey.ipfix");
    Route::get("/sankey/snmp", "DiagramController@sankeySNMP")->name("diagram.sankey.snmp");
    Route::get("/sankey/data/{time}/{type}", "DiagramController@sankeyData");
    Route::get("/overview", "DiagramController@index")->name("diagram.overview");
    Route::get("/overview/data", "DiagramController@data")->name("diagram.overview.data");
});
