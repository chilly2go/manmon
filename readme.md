# Simple Monitoring-Lösung für kleine Netze

Eine einfache Monitoring-Lösung für kleine Netze. 
Ausgelegt auf Nutzung mit Raspberry-Pi Geräten.
Eine Benutzerverwaltung ist nicht vorgesehen, kann aber mit Laravel Bordmitteln ergänzt werden.

## Anforderungen:

 - PHP 7.2
 - Node JS (Webpack: u.a. Sass Compilierung)
 - [Datenbank](https://laravel.com/docs/5.6/database) 
 - Redis (Für Hintergrundverarbeitung)
 - Supervisord (Stellt sicher, dass Jobs zur Hintergrundverarbeitung ausgeführt werden)

## Installation:

- [Composer Installieren](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
- `git clone git@gitlab.com:chilly2go/manmon.git <project-name>`
- Im Projektverzeichnis: 
- - `composer install`
- - `php artisan key:generate` (Schlüssel generieren. Wird für Verschlüsselung durch das Framework verwendet)
- - `npm install`
- - `npm run <mode>` (modes: prod / production / dev / ... Können aus package.json entnommen werden.)
Der Modus "watch" benötigt zu Beginn mehr Zeit, ist aber bei wiederkehrenden Anpassungen zu bevorzugen.

## Einrichtung:

Da in Kombination mit Laravel sehr viele Aufgaben durch das Tool <i>Artisan</i> erledigt werden, empfiehlt sich die Einrichtung eines alias `alias art='php artisan'`.

### Regelmäßige Aufgaben

Regelmäßige Aufgaben werden in Laravel durchgeführt, in dem die durchzuführenden Aufgaben in der Datei `app\Console\Kernel.php` eingetragen werden.
Die Ausführung muss durch einen Cron-Job gestartet werden:

    * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1

Mehr Infos finden sich in der Laravel-Dokumentation zu [Task Scheduling](https://laravel.com/docs/5.6/scheduling)

### Hintergrund-Verarbeitung

Laravel bietet die Möglichkeit, Aufgaben als sog. Jobs zu erstellen und im Hintergrund zu verarbeiten.
Details zu Optionen und Konfiguration findet sich in der [Queues Dokumentation](https://laravel.com/docs/5.6/queues).
Um einen Worker zu starten, der aufgaben der Queue abarbeitet genügt der Aufruf `php artisan queue:work`
Die bereits erwähnte Dokumentation zeigt weitere Nutzungsmöglichkeiten.

Will man sich selbst weniger damit herumschlagen und eventuell eine nette Oberfläche zum Überwachen der Aufgaben, so empfiehlt sich <i>Laravel Horizon</i>.
Horizon wird in diesem Projekt bereits verwendet und als Abhängigkeit angegeben. 
Die [Horizon Dokumentation](https://laravel.com/docs/5.6/horizon) zeigt alle nötigen Schritte und Optionen zur Anpassung.

Eine Beispielkonfiguration für Supervisord um Horizon zu starten:

Datei `/etc/supervisor/conf.d/horizon.conf`

    [program:horizon]
    command=php /var/projects/manmon/artisan horizon
    autostart=true
    autorestart=true
    user=odroid
    redirect_stderr=true
    stdout_logfile=/var/projects/manmon/storage/logs/horizon.log
    stdout_logfile_maxbytes=5242880
    stdout_logfile_backups=3

Um Beispielsweise einen Neustart durchzuführen (zur Übernahme geänderter Dateien für Jobs / Configs) genügt ein Aufruf von `sudo supervisorctl restart horizon`.
Dabei ist zu beachten, dass `root` Berechtigungen zur Interaktion mit Supervisord nötig sind.

## Infos zum PHP Framework:
### About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

### Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

### Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
