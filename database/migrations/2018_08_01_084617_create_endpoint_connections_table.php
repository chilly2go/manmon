<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndpointConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        Schema::create('endpoint_connections',
            function (Blueprint $table)
            {
                $table->increments('id');
                $table->unsignedInteger("ip_from_id");
                $table->unsignedInteger("ip_to_id");
                $table->unsignedInteger("endpoint_interface_index_from")->nullable();
                $table->unsignedInteger("endpoint_interface_index_to")->nullable();
                $table->timestamps();
                $table->unique(["ip_from_id", "ip_to_id"]);
                $table->foreign("ip_from_id")->references("id")->on("i_p__pools");
                $table->foreign("ip_to_id")->references("id")->on("i_p__pools");
                $table->foreign("endpoint_interface_index_from")->references("id")->on("endpoint_connections");
                $table->foreign("endpoint_interface_index_to")->references("id")->on("endpoint_connections");
            });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('endpoint_connections');
    }
}
