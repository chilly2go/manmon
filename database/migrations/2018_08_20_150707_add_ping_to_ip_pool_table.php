<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPingToIpPoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        Schema::table('i_p__pools', function (Blueprint $table)
        {
            $table->unsignedInteger("should_ping")->index()->default(0)
                  ->comment('0=do not ping/1=do ping/2=do ping (external)');
            $table->unsignedBigInteger('latency')->default(0);
            $table->json("data")->comment("additional data if external ip and looked up via api");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::table('i_p__pools', function (Blueprint $table)
        {
            $table->dropColumn('should_ping');
            $table->dropColumn('latency');
            $table->dropColumn('data');
        });
    }
}
