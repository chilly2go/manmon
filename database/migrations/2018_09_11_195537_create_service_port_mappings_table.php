<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicePortMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        Schema::create('service_port_mappings',
            function (Blueprint $table)
            {
                $table->increments('id');
                $table->unsignedInteger('port');
                $table->string('protokoll', 5);
                $table->string('beschreibung', 20);
                $table->boolean('isMalware')->default(0);
                $table->index(['port', 'protokoll']);
            });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('service_port_mappings');
    }
}
