<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnmpPerfDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        Schema::create('snmp_perf_datas',
            function (Blueprint $table)
            {
                $table->bigIncrements('id');
                $table->unsignedInteger("interface_id");
                $table->double("in", NULL, 2);
                $table->double("out", NULL, 2);
                $table->timestamps();
                $table->index("interface_id");
            });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('snmp_perf_datas');
    }
}
