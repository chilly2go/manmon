<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSNMPEndpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        Schema::create('s_n_m_p__endpoints', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger("ip_id")->unique();
            $table->string("community")->nullable(FALSE);
            $table->string("beschreibung")->nullable();
            $table->string("uptime")->nullable();
            $table->string("kontakt")->nullable();
            $table->string("name")->nullable();
            $table->string("ort")->nullable();
            $table->timestamps();
            $table->foreign("ip_id")->references("id")->on("i_p__pools");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('s_n_m_p__endpoints');
    }
}
