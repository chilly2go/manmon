<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpfixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        Schema::create('ipfix',
            function (Blueprint $table)
            {
                $table->bigIncrements('id');
                // $table->increments('id');
                $table->unsignedInteger("bytes");
                $table->unsignedInteger("packets");
                $table->double("duration");
                $table->timestamp("start")->nullable();
                $table->timestamp("end")->nullable();
                $table->unsignedInteger("from_ip");
                $table->unsignedInteger("to_ip");
                $table->integer("from_port");
                $table->integer("to_port");
                $table->string('protokoll', 5);
                $table->string("flags", 10);
                $table->boolean("isFeatureCalucalted")->default(0);
                $table->unsignedInteger("26")->default(0);
                $table->unsignedInteger("27")->default(0);
                $table->unsignedInteger("30")->default(0);
                $table->unsignedInteger("31")->default(0);
                $table->unsignedInteger("34")->default(0);
                $table->unsignedInteger("35")->default(0);
                $table->unsignedInteger("38")->default(0);
                $table->timestamps();
                $table->index("isFeatureCalucalted");
                $table->index("start");
                $table->index("to_ip");
                $table->index("to_port");
                $table->index("from_port");
                $table->index("from_ip");
                $table->foreign('from_ip')->references('id')->on('i_p__pools');
                $table->foreign('to_ip')->references('id')->on('i_p__pools');
            });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('ipfix');
    }
}
