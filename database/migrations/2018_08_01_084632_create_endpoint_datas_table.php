<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndpointDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        Schema::create('endpoint_datas', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger("endpoint_id");
            $table->unsignedInteger("mtu")->nullable();
            $table->unsignedBigInteger("speed")->nullable();
            $table->string("mac", 18)->nullable();
            $table->string("name")->nullable();
            $table->unsignedSmallInteger("index");
            $table->text("values")->nullable();
            $table->timestamps();
            $table->foreign("endpoint_id")->references("id")->on("s_n_m_p__endpoints");
            $table->unique(['endpoint_id','index']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('endpoint_datas');
    }
}
