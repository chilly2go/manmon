<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpfixFeatureTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public
    function up()
    {
        
        // DB::unprepared('
        // CREATE TRIGGER tr_IPFix_Calc_Features AFTER INSERT ON `users` FOR EACH ROW
        //     BEGIN
        //         UPDATE ipfix
        //         Cross Join (select distinct fix.id,
        //                                     fix.start,
        //                                     sum(distinct w200.to_ip = fix.to_ip)                                        as `26`,
        //                                     sum(distinct w200.to_ip = fix.to_ip and w200.to_port = fix.to_port)         as `27`,
        //                                     sum(distinct w200.to_port = fix.to_port)                                    as `30`,
        //                                     sum(distinct w200.to_port = fix.to_port and w200.from_port = fix.from_port) as `31`,
        //                                     sum(distinct t10.to_ip = fix.to_ip)                                         as `34`,
        //                                     sum(distinct t10.to_ip = fix.to_ip and t10.to_port = fix.to_port)           as `35`,
        //                                     sum(distinct t10.to_port = fix.to_port)                                     as `38`
        //                     from ipfix fix
        //                            join i_p__pools to_ip on to_ip.id = fix.to_ip
        //                            join i_p__pools from_ip on from_ip.id = fix.from_ip
        //                            join (SELECT distinct id, start, to_ip, to_port FROM ipfix) t10
        //                              ON t10.start between date_sub(fix.start, interval 10 second) and fix.start
        //                            join (SELECT distinct start, to_ip, to_port, from_port FROM ipfix LIMIT 200) w200
        //                              ON w200.start between date_sub(fix.start, interval 5 hour) and fix.start
        //                     where fix.id = NEW.id
        //                     GROUP BY fix.id, fix.start
        //                     ORDER BY fix.id) As Z
        //         Set ipfix.`26`          = Z.`26`,
        //             ipfix.`27`          = Z.`27`,
        //             ipfix.`30`          = Z.`30`,
        //             ipfix.`31`          = Z.`31`,
        //             ipfix.`34`          = Z.`34`,
        //             ipfix.`35`          = Z.`35`,
        //             ipfix.`38`          = Z.`38`,
        //             isFeatureCalucalted = 1
        //         Where ipfix.id = NEW.id;
        //     END
        // ');
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        // DB::unprepared('DROP TRIGGER `tr_IPFix_Calc_Features`');
    }
}
